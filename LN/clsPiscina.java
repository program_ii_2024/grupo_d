package LN;

import COMUN.clsConstantes.en_Rol;
import COMUN.itfProperty;
import Excepciones.clsOpcionNoValida;

import static COMUN.clsConstantes.STR_PISCINA;
import static COMUN.clsConstantes.STR_TAMANO_PICINA;
import static COMUN.clsConstantes.STR_CALLES_PISCINA;

/**
 * Esta clase representa una piscina en el sistema. Extiende la clase
 * clsEstablecimientoLN para heredar sus atributos y metodos.
 */

public class clsPiscina extends clsEstablecimiento implements itfProperty
{
    private String str_piscina;
    private float float_temperatura;
    private int int_tamano;
    private int int_calles;

    /**
     * Constructor de la clase clsPiscina.
     * 
     * @param str_Piscina El nombre de la piscina.
     * @param float_temperatura La temperatura de la piscina.
     * @param int_tamano El tamaño de la piscina.
     * @param int_calles El numero de calles en la piscina.
     */
    public clsPiscina(String str_Piscina, float float_temperatura, 
        int int_tamano, int int_calles)
    {
        this.str_piscina = str_Piscina;
        this.float_temperatura = float_temperatura;
        this.int_tamano = int_tamano;
        this.int_calles = int_calles;
    }

    /**
     * Constructor vacio de la clase clsgimnasio
     * 
     */
    public clsPiscina()
    {

    }

    /**
     * Obtiene el DNI asociado a la reserva de la piscina.
     *
     * @return El DNI asociado a la reserva de la piscina.
     */
    public String getStr_piscina()
    {
        return this.str_piscina;
    }

    /**
     * Establece el DNI asociado a la reserva de la piscina.
     *
     * @param str_nombre El nuevo DNI asociado a la reserva de la piscina.
     */
    public void setStr_piscina(String str_nombre)
    {
        this.str_piscina = str_nombre;
    }

    /**
     * Obtiene el valor de la variable "float_temperatura".
     * 
     * @return El valor actual de la temperatura.
     */

    public float getFloat_temperatura()
    {
        return float_temperatura;
    }

    /**
     * Establece el valor de la variable "float_temperatura".
     * 
     * @param float_temperatura El nuevo valor para la temperatura.
     */
    public void setFloat_temperatura(float float_temperatura)
    {
        this.float_temperatura = float_temperatura;
    }

    /**
     * Obtiene el valor de la variable "int_tamano".
     * 
     * @return El valor actual del tamaño.
     */
    public int getInt_tamano()
    {
        return int_tamano;
    }

    /**
     * Establece el valor de la variable "int_tamano".
     * 
     * @param int_tamano El nuevo valor para el tamaño.
     */
    public void setInt_tamano(int int_tamano)
    {
        this.int_tamano = int_tamano;
    }

    /**
     * Obtiene el valor de la variable "int_calles".
     * 
     * @return El valor actual del numero de calles.
     */

    public int getInt_calles()
    {
        return int_calles;
    }

    /**
     * Establece el valor de la variable "int_calles".
     * 
     * @param int_calles El nuevo valor para el numero de calles.
     */

    public void setInt_calles(int int_calles)
    {
        this.int_calles = int_calles;
    }

    /**
     * Sobrescribe el metodo toString para representar el usuario como una 
     * cadena de texto.
     *
     * @return Una cadena que representa al usuario el nombre, tamano y calles.
     */

    public String toString()
    {
        String str_retorno;
        str_retorno = this.str_piscina;
        str_retorno = str_retorno + " " + this.int_tamano;
        str_retorno = str_retorno + " " + this.int_calles;
        return str_retorno;
    }

    /**
     * Calcula el codigo hash para este objeto.
     * 
     * @return El valor del codigo hash.
     */

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + 
            ((str_piscina == null) ? 0 : str_piscina.hashCode());
        return result;
    }

    /**
     * Comprueba si este objeto es igual a otro objeto.
     * 
     * @param obj El objeto con el que se compara.
     * @return true si los objetos son iguales, false en caso contrario.
     */

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        clsPiscina other = (clsPiscina) obj;
        if (str_piscina == null)
        {
            if (other.str_piscina != null)
            {
                return false;
            }
        } else if (!str_piscina.equals(other.str_piscina))
        {
            return false;
        }
        return true;
    }

    /**
     * Obtiene el valor de la propiedad especificada.
     * 
     * @param str_propiedad El nombre de la propiedad que se desea obtener.
     * @return El valor actual de la propiedad especificada.
     */
    @Override
    public Object getProperty(String str_propiedad)
    {
        switch (str_propiedad)
        {
        case STR_PISCINA:
            return this.str_piscina;
        case STR_TAMANO_PICINA:
            return this.int_tamano;
        case STR_CALLES_PISCINA:
            return this.int_calles;
        default:
            throw new clsOpcionNoValida("Esa propiedad de piscina" + 
                " no existe.Se cierra el programa");
        }

    }

    /**
     * Obtiene el rol del usuario.
     * 
     * @return El rol actual del usuario.
     */

    @Override
    public en_Rol get_en_Rol()
    {
        throw new UnsupportedOperationException("No esta programado");
    }
}
