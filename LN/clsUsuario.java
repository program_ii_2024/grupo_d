package LN;

import COMUN.itfProperty;
import Excepciones.clsOpcionNoValida;
import COMUN.clsConstantes.en_Rol;
import static COMUN.clsConstantes.STR_DNI;
import static COMUN.clsConstantes.STR_NOMBRE;
import static COMUN.clsConstantes.STR_CONTRASENA;
import static COMUN.clsConstantes.STR_APELLIDO;

/**
 * Esta clase representa un usuario del sistema.
 */

public class clsUsuario implements itfProperty
{
    private String str_nombre;
    private String str_apellido;
    private String str_dni;
    private String str_contrasena;
    private en_Rol en_rol;

    /**
     * Constructor vacio de la clase clsUsuario
     * 
     */
    public clsUsuario()
    {
    }

    /**
     * Constructor de la clase clsUsuarioLN.
     * 
     * @param str_nombre Nombre del usuario.
     * @param str_apellido Apellido del usuario.
     * @param str_dni DNI del usuario.
     * @param str_contrasena Contraseña del usuario.
     * @param en_rol Rol del usuario.
     */

    public clsUsuario(String str_nombre, String str_apellido, String str_dni, 
        String str_contrasena, en_Rol en_rol)
    {
        this.str_nombre = str_nombre;
        this.str_apellido = str_apellido;
        this.str_dni = str_dni;
        this.str_contrasena = str_contrasena;
        this.en_rol = en_rol;
    }

    /**
     * Obtiene el nombre del usuario.
     * 
     * @return El nombre del usuario.
     */

    public String getNombre()
    {
        return this.str_nombre;
    }

    /**
     * Establece el nombre del usuario.
     * 
     * @param str_nombre El nuevo nombre del usuario.
     */

    public void setNombre(String str_nombre)
    {
        this.str_nombre = str_nombre;
    }

    /**
     * Obtiene el apellido del usuario.
     * 
     * @return El apellido del usuario.
     */

    public String getApellido()
    {
        return this.str_apellido;
    }

    /**
     * Establece el apellido del usuario.
     * 
     * @param str_apellido El nuevo apellido del usuario.
     */

    public void setApellido(String str_apellido)
    {
        this.str_apellido = str_apellido;
    }

    /**
     * Obtiene el DNI del usuario.
     * 
     * @return El DNI del usuario.
     */

    public String getDNI()
    {
        return this.str_dni;
    }

    /**
     * Establece el DNI del usuario.
     * 
     * @param str_dni El nuevo DNI del usuario.
     */

    public void setDNI(String str_dni)
    {
        this.str_dni = str_dni;
    }

    /**
     * Obtiene la contrasena del usuario.
     * 
     * @return La contrasena del usuario.
     */

    public String getContrasena()
    {
        return this.str_contrasena;
    }

    /**
     * Establece la contraseña del usuario.
     * 
     * @param str_contrasena La nueva contraseña del usuario.
     */

    public void setContrasena(String str_contrasena)
    {
        this.str_contrasena = str_contrasena;
    }

    /**
     * Obtiene el rol del usuario.
     * 
     * @return El rol del usuario.
     */

    public en_Rol getRol()
    {
        return this.en_rol;
    }

    /**
     * Establece el rol del usuario.
     * 
     * @param en_rol El nuevo rol del usuario.
     */
    public void setRol(en_Rol en_rol)
    {
        this.en_rol = en_rol;
    }

    /**
     * Sobrescribe el metodo toString para representar el usuario como una 
     * cadena de texto.
     * 
     * @return Cadena que representa al usuario con su nombre, apellido y DNI.
     */
    public String toString()
    {
        String str_retorno;
        str_retorno = this.str_nombre;
        str_retorno = str_retorno + " " + this.str_apellido;
        str_retorno = str_retorno + " " + this.str_dni;
        return str_retorno;
    }

    /**
     * Obtiene el valor de una propiedad especifica.
     *
     * @param str_propiedad El nombre de la propiedad.
     * @return El valor correspondiente a la propiedad o null si no se encuentra.
     */

    @Override
    public Object getProperty(String str_propiedad)
    {
        switch (str_propiedad)
        {
        case STR_DNI:
            return str_dni;
        case STR_NOMBRE:
            return str_nombre;
        case STR_CONTRASENA:
            return str_contrasena;
        case STR_APELLIDO:
            return str_apellido;
        default:
            throw new clsOpcionNoValida("Esa propiedad del" + 
                " usuario no existe.Se cierra el programa");
        }

    }

    /**
     * Devuelve el rol en_Rol asociado a esta instancia.
     *
     * @return El rol en_Rol.
     */

    @Override
    public en_Rol get_en_Rol()
    {
        return this.en_rol;
    }
}
