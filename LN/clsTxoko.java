package LN;

import COMUN.clsConstantes.en_Rol;
import COMUN.itfProperty;
import Excepciones.clsOpcionNoValida;

import static COMUN.clsConstantes.STR_TXOKO;
import static COMUN.clsConstantes.STR_MESAS;
import static COMUN.clsConstantes.STR_PRESIDENTE_TXOKO;

/**
 * Esta clase representa un txoko que puede ser reservado. Extiende la clase
 * clsEstablecimientoLN para heredar sus atributos y metodos.
 */
public class clsTxoko extends clsEstablecimiento implements itfProperty
{
    private String str_presidente;
    private String str_txoko;
    private int int_mesas;

    /**
     * Constructor vacio de la clase clsTxoko
     * 
     */
    public clsTxoko()
    {

    }

    /**
     * Crea una instancia de la clase `clsTxoko`.
     *
     * @param str_presidente El nombre del presidente del txoko.
     * @param str_txoko El nombre del txoko.
     * @param int_mesas El numero de mesas en el txoko.
     */
    public clsTxoko(String str_presidente, String str_txoko, int int_mesas)
    {
        this.str_presidente = str_presidente;
        this.str_txoko = str_txoko;
        this.int_mesas = int_mesas;
    }

    /**
     * Obtiene el DNI asociado a la reserva del txoko.
     * 
     * @return El DNI asociado a la reserva del txoko.
     */
    public String getStr_presidente()
    {
        return this.str_presidente;
    }

    /**
     * Establece el DNI asociado a la reserva del txoko.
     * 
     * @param str_presidente El nombre del presidente del txoko.
     */
    public void setStr_presidente(String str_presidente)
    {
        this.str_presidente = str_presidente;
    }

    /**
     * Obtiene el valor actual de `int_mesas`.
     *
     * @return El valor actual de `int_mesas`.
     */
    public int getInt_mesas()
    {
        return int_mesas;
    }

    /**
     * Establece el valor de `int_mesas`.
     *
     * @param int_mesas El nuevo valor para `int_mesas`.
     */
    public void setInt_mesas(int int_mesas)
    {
        this.int_mesas = int_mesas;
    }

    /**
     * Obtiene el valor actual de `str_txoko`.
     *
     * @return El valor actual de `str_txoko`.
     */
    public String getStr_txoko()
    {
        return str_txoko;
    }

    /**
     * Establece el valor de `str_txoko`.
     *
     * @param str_txoko El nuevo valor para `str_txoko`.
     */
    public void setStr_txoko(String str_txoko)
    {
        this.str_txoko = str_txoko;
    }

    /**
     * Sobrescribe el metodo toString para representar el usuario como una 
     * cadena de texto.
     *
     * @return Una cadena que representa al usuario con el nombre, presidente y
     * numero de mesas.
     */
    public String toString()
    {
        String str_retorno;
        str_retorno = this.str_txoko;
        str_retorno = str_retorno + " " + this.str_presidente;
        str_retorno = str_retorno + " " + this.int_mesas;
        return str_retorno;
    }

    /**
     * Devuelve la propiedad especificada en español.
     * 
     * @param str_propiedad El nombre de la propiedad.
     * @return El valor de la propiedad o null si no se encuentra.
     */
    @Override
    public Object getProperty(String str_propiedad)
    {
        switch (str_propiedad)
        {
        case STR_TXOKO:
            return this.str_txoko;
        case STR_PRESIDENTE_TXOKO:
            return this.str_presidente;
        case STR_MESAS:
            return this.int_mesas;
        default:
            throw new clsOpcionNoValida("Esa propiedad de txoko" + 
                " no existe.Se cierra el programa");

        }

    }

    /**
     * Devuelve el rol actual.
     *
     * @return El rol actual.
     * @throws UnsupportedOperationException Si el metodo no esta implementado.
     */
    @Override
    public en_Rol get_en_Rol()
    {
        throw new UnsupportedOperationException("No esta programado");
    }
}