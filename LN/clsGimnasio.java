package LN;

import COMUN.clsConstantes.en_Rol;
import Excepciones.clsOpcionNoValida;
import COMUN.itfProperty;
import static COMUN.clsConstantes.STR_GIMNASIO;
import static COMUN.clsConstantes.STR_AFORO_GIMNASIO;
import static COMUN.clsConstantes.STR_MAQUINAS_GIMNASIO;

/**
 * Esta clase representa un gimnasio en el sistema. Extiende la clase
 * clsEstablecimientoLN para heredar sus atributos y metodos.
 */

public class clsGimnasio extends clsEstablecimiento implements itfProperty, 
    Comparable<clsGimnasio>
{
    private int int_aforo;
    private int int_maquinas;
    private String str_gimnasio;

    /**
     * Constructor vacio de la clase clsgimnasio
     * 
     */
    public clsGimnasio()
    {

    }

    /**
     * Constructor para crear un nuevo objeto `Gimnasio`.
     *
     * @param int_aforo El aforo del gimnasio.
     * @param int_maquinas El numero de maquinas en el gimnasio.
     * @param str_gimnasio El nombre del gimnasio.
     */
    public clsGimnasio(int int_aforo, int int_maquinas, String str_gimnasio)
    {
        this.int_aforo = int_aforo;
        this.int_maquinas = int_maquinas;
        this.str_gimnasio = str_gimnasio;
    }

    /**
     * Obtiene el valor de la variable "str_gimnasio".
     * 
     * @return El valor actual de "str_gimnasio".
     */
    public String getStr_gimnasio()
    {
        return str_gimnasio;
    }

    /**
     * Establece el valor de la variable "str_gimnasio".
     * 
     * @param str_Gimnasio El nuevo valor del gimnasio.
     */
    public void setStr_gimnasio(String str_Gimnasio)
    {
        this.str_gimnasio = str_Gimnasio;
    }

    /**
     * Obtiene el valor de la variable "int_maquinas".
     * 
     * @return El valor actual de las maquinas.
     */
    public int getInt_maquinas()
    {
        return int_maquinas;
    }

    /**
     * Establece el valor de la variable "int_maquinas".
     * 
     * @param int_maquinas El nuevo valor de las maquinas.
     */
    public void setInt_maquinas(int int_maquinas)
    {
        this.int_maquinas = int_maquinas;
    }

    /**
     * Obtiene el valor del aforo.
     * 
     * @return El valor actual del aforo.
     */
    public int getInt_aforo()
    {
        return int_aforo;
    }

    /**
     * Establece el valor del aforo.
     * 
     * @param int_aforo El nuevo valor del aforo.
     */
    public void setInt_aforo(int int_aforo)
    {
        this.int_aforo = int_aforo;
    }

    /**
     * Retorna una representacion en cadena del objeto.
     * 
     * @return Una cadena que representa el objeto, que en este caso es el 
     * gimnasio que es.
     *
     */
    public String toString()
    {
        String str_retorno;
        str_retorno = this.str_gimnasio;
        str_retorno = str_retorno + " " + this.int_aforo;
        str_retorno = str_retorno + " " + this.int_maquinas;
        return str_retorno;
    }

    /**
     * Compara este gimnasio con otro gimnasio en funcion de su nombre y 
     * cantidad de maquinas. Si el argumento es nulo, devuelve 1. Si este 
     * gimnasio es igual al otro gimnasio, devuelve 0. Si el nombre de este 
     * gimnasio es diferente del nombre del otro gimnasio, devuelve la 
     * comparacion de los nombres en orden inverso. Si los nombres son iguales, 
     * compara la cantidad de maquinas en orden inverso.
     *
     * @param arg0 El gimnasio con el que se va a comparar.
     * @return Un valor negativo si este gimnasio es menor que el otro gimnasio,
     * cero si son iguales, o un valor positivo si este gimnasio es mayor que el
     * otro gimnasio.
     */
    public int compareTo(clsGimnasio arg0)
    {
        if (arg0 == null)
        {
            return 1;
        }

        if (this.equals(arg0))
        {
            return 0;
        }

        if (!this.str_gimnasio.equals(arg0.getStr_gimnasio()))
        {
            return this.str_gimnasio.compareTo(arg0.getStr_gimnasio()) * (-1);
        }
        Integer int_maquina = this.int_maquinas;
        return int_maquina.compareTo(arg0.getInt_maquinas()) * (-1);
    }

    /**
     * Obtiene el valor de la variable "str_gimnasio".
     * 
     * @param str_propiedad El nombre de la propiedad que se desea obtener.
     * @return El valor actual de la propiedad especificada.
     */
    @Override
    public Object getProperty(String str_propiedad)
    {
        switch (str_propiedad)
        {
        case STR_GIMNASIO:
            return this.str_gimnasio;
        case STR_AFORO_GIMNASIO:
            return this.int_aforo;
        case STR_MAQUINAS_GIMNASIO:
            return this.int_maquinas;
        default:
            throw new clsOpcionNoValida("Esa propiedad de Gimnasio" + 
                "no existe.Se cierra el programa");
        }
    }

    /**
     * Obtiene el valor del rol del usuario.
     * 
     * @return El rol actual del usuario.
     */
    @Override
    public en_Rol get_en_Rol()
    {
        throw new UnsupportedOperationException("Eso no esta" + "programado");
    }
}