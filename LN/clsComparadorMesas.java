package LN;

import java.util.Comparator;

/**
 * Clase comparadora que implementa el interfaz Comparator para incluir un
 * segundo criterio demordenacion, ademas de la ordenacion natural. En este caso
 * se ordenan los txokos por numero de mesas.
 */

public class clsComparadorMesas implements Comparator<clsTxoko>
{
    /**
     * Metodo que recibe dos txokos y compara el numero de mesas
     * 
     * @param obj_Txoko1 Primer txoko
     * @param obj_Txoko2 Segundo txoko
     * @return Devuelve primero el txoko que mas mesas tenga
     */
    public int compare(clsTxoko obj_Txoko1, clsTxoko obj_Txoko2)
    {
        return ((Integer) obj_Txoko2.getInt_mesas()).compareTo
            (obj_Txoko1.getInt_mesas());
    }
}
