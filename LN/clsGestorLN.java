package LN;

import static COMUN.clsConstantesBD.TABLA_PISCINA_TAMANO;
import static COMUN.clsConstantesBD.TABLA_PISCINA_CALLES;
import static COMUN.clsConstantesBD.TABLA_PISCINA_TEMPERATURA;
import static COMUN.clsConstantesBD.TABLA_PISCINA_PISCINA;
import static COMUN.clsConstantesBD.TABLA_TXOKO_MESAS;
import static COMUN.clsConstantesBD.TABLA_TXOKO_PRESIDENTE;
import static COMUN.clsConstantesBD.TABLA_TXOKO_TXOKO;
import static COMUN.clsConstantesBD.TABLA_GIMNASIO_AFORO;
import static COMUN.clsConstantesBD.TABLA_GIMNASIO_MAQUINAS;
import static COMUN.clsConstantesBD.TABLA_GIMNASIO_GIMNASIO;
import static COMUN.clsConstantesBD.TABLA_USUARIO_APELLIDO;
import static COMUN.clsConstantesBD.TABLA_USUARIO_CONTRASENA;
import static COMUN.clsConstantesBD.TABLA_USUARIO_DNI;
import static COMUN.clsConstantesBD.TABLA_USUARIO_NOMBRE;
import static COMUN.clsConstantesBD.TABLA_USUARIO_ROL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

import java.util.Collections;

import COMUN.itfProperty;
import COMUN.clsConstantes.en_Rol;
import Excepciones.clsDniErroneo;
import LD.clsGestorLD;

/**
 * Esta clase proporciona metodos para gestionar usuarios, piscinas gimansisos y
 * txokos.
 */
public class clsGestorLN implements itfProperty
{

    /**
     * HashMap que almacena una lista de usuarios donde la clave es el DNI del
     * usuario y el valor es un objeto clsUsuario.
     */
    private HashMap<String, clsUsuario> map_listaUsuarios;

    /**
     * HashSet que almacena una lista de piscinas.
     */
    private HashSet<clsPiscina> set_listaPiscinas;

    /**
     * TreeSet que almacena una lista de gimnasios, ordenados automaticamente 
     * por su comparador natural.
     */
    private TreeSet<clsGimnasio> set_listaGimnasios;

    /**
     * ArrayList que almacena una lista de txokos.
     */
    private ArrayList<clsTxoko> arrl_listaTxokos;

    /**
     * Objeto de la clase clsGestorLD, utilizado para interactuar con la capa de
     * acceso a datos.
     */
    private clsGestorLD obj_clsGestorLD;

    /**
     * Constructor de la clase clsGestorLN. Inicializa todas las listas del 
     * programa y crea usuarios, piscinas, gimansios y txokos por defecto.
     */
    public clsGestorLN()
    {
        map_listaUsuarios = new HashMap<String, clsUsuario>();
        set_listaPiscinas = new HashSet<clsPiscina>();
        set_listaGimnasios = new TreeSet<clsGimnasio>();
        arrl_listaTxokos = new ArrayList<clsTxoko>();
        obj_clsGestorLD = new clsGestorLD();
    }

    /**
     * Crea un nuevo usuario y lo agrega a la lista de usuarios. Añadimos 
     * excepcion explicita para comprobar antes si el dni es valido
     * 
     * @param str_nombre Nombre del usuario.
     * @param str_apellido Apellido del usuario.
     * @param str_dni DNI del usuario.
     * @param str_contrasena Contrasena del usuario.
     * @param en_permisosRol Rol del usuario.
     * @throws clsDniErroneo Excepcion si el dni es incorrecto.
     */
    public void vo_usuarioNuevo(String str_nombre, String str_apellido, 
        String str_dni, String str_contrasena,
        en_Rol en_permisosRol) throws clsDniErroneo
    {
        if (str_dni.length() == 9 || str_dni == "admin")
        {
            clsUsuario obj_usuarioNuevo;
            String str_id;
            obj_usuarioNuevo = new clsUsuario(str_nombre, str_apellido, 
                str_dni, str_contrasena, en_permisosRol);
            map_listaUsuarios.put(str_dni, obj_usuarioNuevo);

            this.obj_clsGestorLD.vo_connect();
            str_id = obj_clsGestorLD.str_insertarUsuario(str_dni, str_nombre, 
                str_apellido, str_contrasena,
                en_permisosRol);
            System.out.println("DEBUG:Nuevo dni: " + str_id);
            this.obj_clsGestorLD.vo_disconnect();
        } else
        {
            throw new clsDniErroneo(str_dni);
        }
    }

    /**
     * Obtiene la lista de usuarios de la BBDD.
     *
     * @return Dice si dicha operacion se ha relizado con exito
     */
    public boolean bln_descargarUsuariosBD()
    {
        ResultSet rs_usuarios;
        clsUsuario obj_aux = new clsUsuario();
        this.obj_clsGestorLD.vo_connect();
        rs_usuarios = this.obj_clsGestorLD.rs_listadoUsuarios();
        if (rs_usuarios != null)
        {
            try
            {
                while (rs_usuarios.next())
                {
                    obj_aux.setNombre(rs_usuarios.getString
                        (TABLA_USUARIO_NOMBRE));
                    obj_aux.setApellido(rs_usuarios.getString
                        (TABLA_USUARIO_APELLIDO));
                    obj_aux.setDNI(rs_usuarios.getString
                        (TABLA_USUARIO_DNI));
                    obj_aux.setContrasena(rs_usuarios.getString
                        (TABLA_USUARIO_CONTRASENA));
                    String str_rol = rs_usuarios.getString(TABLA_USUARIO_ROL);
                    switch (str_rol)
                    {
                    case "en_Rol_ADMIN":
                        obj_aux.setRol(en_Rol.en_Rol_ADMIN);
                        break;
                    case "en_Rol_READ":
                        obj_aux.setRol(en_Rol.en_Rol_READ);
                        break;
                    case "en_Rol_READ_WRITE":
                        obj_aux.setRol(en_Rol.en_Rol_READ_WRITE);
                        break;
                    case "en_Rol_INVALID":
                        obj_aux.setRol(en_Rol.en_Rol_INVALID);
                        break;
                    default:
                        obj_aux.setRol(en_Rol.en_Rol_INVALID);
                        break;
                    }
                    map_listaUsuarios.put(obj_aux.getDNI(), obj_aux);
                }
            } catch (SQLException e)
            {
                System.out.println("DEBUG: Error en rs_cargar usuarios: " + e);
            }
            return false;
        }
        this.obj_clsGestorLD.vo_disconnect();
        return true;
    }

    /**
     * Obtiene la lista de txokos de la BBDD.
     *
     * @return Dice si dicha operacion se ha relizado con exito
     */
    public boolean bln_descargarTxokosBD()
    {
        ResultSet rs_txokos;
        clsTxoko obj_aux;
        this.obj_clsGestorLD.vo_connect();
        rs_txokos = this.obj_clsGestorLD.rs_listadoTxokos();
        if (rs_txokos != null)
        {
            try
            {
                while (rs_txokos.next())
                {
                    obj_aux = new clsTxoko();
                    obj_aux.setStr_presidente(rs_txokos.getString
                        (TABLA_TXOKO_PRESIDENTE));
                    obj_aux.setStr_txoko(rs_txokos.getString
                        (TABLA_TXOKO_TXOKO));
                    obj_aux.setInt_mesas(rs_txokos.getInt
                        (TABLA_TXOKO_MESAS));
                    arrl_listaTxokos.add(obj_aux);
                }
            } catch (SQLException e)
            {
                System.out.println("DEBUG: Error en rs_cargar txokos: " + e);
            }
            return false;
        }
        this.obj_clsGestorLD.vo_disconnect();
        return true;
    }

    /**
     * Descarga la informacion de los gimnasios desde la base de datos.
     *
     * @return true si la descarga se realizo correctamente, false en caso
     * contrario.
     */
    public boolean bln_descargarGimnasiosBD()
    {
        ResultSet rs_gimnasios;
        clsGimnasio obj_aux1;
        this.obj_clsGestorLD.vo_connect();
        rs_gimnasios = this.obj_clsGestorLD.rs_listadoGimasios();
        if (rs_gimnasios != null)
        {
            try
            {
                while (rs_gimnasios.next())
                {
                    obj_aux1 = new clsGimnasio();
                    obj_aux1.setInt_aforo(rs_gimnasios.getInt
                        (TABLA_GIMNASIO_AFORO));
                    obj_aux1.setInt_maquinas(rs_gimnasios.getInt
                        (TABLA_GIMNASIO_MAQUINAS));
                    obj_aux1.setStr_gimnasio(rs_gimnasios.getString
                        (TABLA_GIMNASIO_GIMNASIO));
                    set_listaGimnasios.add(obj_aux1);
                }
            } catch (SQLException e)
            {
                System.out.println("DEBUG: Error en rs_cargar Gimnasios: " + e);
            }
            return false;
        }
        this.obj_clsGestorLD.vo_disconnect();

        return true;
    }

    /**
     * Descarga la información de las piscinas desde la base de datos.
     *
     * @return true si la descarga se realizo correctamente, false en caso
     * contrario.
     */
    public boolean bln_descargarPiscinasBD()
    {
        ResultSet rs_piscinas;
        this.obj_clsGestorLD.vo_connect();
        rs_piscinas = this.obj_clsGestorLD.rs_listadoPiscinas();
        if (rs_piscinas != null)
        {
            try
            {
                while (rs_piscinas.next())
                {
                    clsPiscina obj_aux1 = new clsPiscina();
                    obj_aux1.setStr_piscina(rs_piscinas.getString
                        (TABLA_PISCINA_PISCINA));
                    obj_aux1.setFloat_temperatura(rs_piscinas.getFloat
                        (TABLA_PISCINA_TEMPERATURA));
                    obj_aux1.setInt_tamano(rs_piscinas.getInt
                        (TABLA_PISCINA_TAMANO));
                    obj_aux1.setInt_calles(rs_piscinas.getInt
                        (TABLA_PISCINA_CALLES));
                    set_listaPiscinas.add(obj_aux1);
                }
            } catch (SQLException e)
            {
                System.out.println("DEBUG: Error en rs_cargar Gimnasios: " + e);
            }
            return false;
        }
        this.obj_clsGestorLD.vo_disconnect();
        return true;
    }

    /**
     * Obtiene la lista de usuarios.
     *
     * @return La lista de usuarios.
     */
    public ArrayList<itfProperty> arrl_mostrarUsuarios()
    {
        ResultSet rs_usuarios;
        ArrayList<itfProperty> arrl_usuarios;
        itfProperty obj_auxUsuario;
        clsUsuario obj_aux;

        arrl_usuarios = new ArrayList<>();
        this.obj_clsGestorLD.vo_connect();
        rs_usuarios = this.obj_clsGestorLD.rs_listadoUsuarios();
        if (rs_usuarios != null)
        {
            try
            {
                while (rs_usuarios.next())
                {
                    obj_aux = new clsUsuario();
                    obj_aux.setNombre(rs_usuarios.getString
                        (TABLA_USUARIO_NOMBRE));
                    obj_aux.setApellido(rs_usuarios.getString
                        (TABLA_USUARIO_APELLIDO));
                    obj_aux.setDNI(rs_usuarios.getString
                        (TABLA_USUARIO_DNI));
                    obj_auxUsuario = obj_aux;
                    arrl_usuarios.add(obj_auxUsuario);
                }
            } catch (SQLException e)
            {
                System.out.println("DEBUG: Error en rs_cargar usuarios: " + e);
            }
        }
        this.obj_clsGestorLD.vo_disconnect();
        return arrl_usuarios;
    }

    /**
     * Devuelve una lista de propiedades de gimnasios disponibles.
     *
     * @return La lista de propiedades de gimnasios.
     */
    public TreeSet<itfProperty> set_mostrarGimnasios()
    {
        TreeSet<itfProperty> set_listaAux;
        set_listaAux = new TreeSet<itfProperty>();
        for (clsGimnasio obj_auxGimnasio : set_listaGimnasios)
            set_listaAux.add(obj_auxGimnasio);
        return set_listaAux;
    }

    /**
     * Devuelve una lista de propiedades de Txokos disponibles.
     *
     * @return La lista de propiedades de Txokos.
     */
    public ArrayList<itfProperty> arrl_mostrarTxokos()
    {
        ArrayList<itfProperty> arrl_listaAux;
        arrl_listaAux = new ArrayList<itfProperty>();
        for (clsTxoko obj_auxTxokos : arrl_listaTxokos)
            arrl_listaAux.add(obj_auxTxokos);
        return arrl_listaAux;
    }

    /**
     * Devuelve una lista de propiedades de Txokos disponibles.
     *
     * @return La lista de propiedades de Txokos ordenados.
     */

    public ArrayList<itfProperty> arrl_mostrarTxokosOrdenados()
    {
        ArrayList<itfProperty> arrl_listaAux;
        arrl_listaAux = new ArrayList<itfProperty>();
        Collections.sort(arrl_listaTxokos, new clsComparadorMesas());
        for (clsTxoko obj_auxTxokos : arrl_listaTxokos)
            arrl_listaAux.add(obj_auxTxokos);
        return arrl_listaAux;
    }

    /**
     * Devuelve un conjunto (HashSet) de propiedades de piscinas disponibles.
     *
     * @return El conjunto de propiedades de piscinas.
     */

    public HashSet<itfProperty> set_mostrarPiscinas()
    {
        HashSet<itfProperty> set_listaAux;
        set_listaAux = new HashSet<itfProperty>();
        for (clsPiscina obj_auxPiscinas : this.set_listaPiscinas)
        {
            set_listaAux.add(obj_auxPiscinas);
        }
        return set_listaAux;
    }

    /**
     * Inicia sesion de un usuario en el sistema.
     * 
     * @param str_dni DNI del usuario.
     * @param str_contrasena Contrasena del usuario.
     * @return El objeto clsUsuarioLN si la sesion se inicia correctamente, de 
     * lo contrario null.
     */
    public itfProperty obj_inicioCuenta(String str_dni, String str_contrasena)
    {
        itfProperty obj_usuarioIniciado;
        obj_usuarioIniciado = null;
        clsUsuario usuario;
        usuario = map_listaUsuarios.get(str_dni);
        if (usuario != null)
        {
            if (usuario.getContrasena().equals(str_contrasena))
            {
                obj_usuarioIniciado = usuario;
            }
            return obj_usuarioIniciado;
        }
        return obj_usuarioIniciado;
    }

    /**
     * Comprueba si el DNI proporcionado coincide con un usuario registrado.
     *
     * @param str_dni El numero de DNI a verificar.
     * @return `true` si el DNI coincide con un usuario registrado, false en 
     * caso contrario.
     */
    public boolean bln_comprobarDni(String str_dni)
    {
        boolean bln_coincide;
        bln_coincide = false;
        clsUsuario obj_usuario = null;
        obj_usuario = map_listaUsuarios.get(str_dni);
        if (obj_usuario != null)
        {
            bln_coincide = true;
        }
        return bln_coincide;
    }

    /**
     * Elimina un usuario del sistema.
     * 
     * @param str_dni DNI del usuario a eliminar.
     */
    public void vo_eliminarPersona(String str_dni)
    {
        String str_dniEliminado;
        this.map_listaUsuarios.remove(str_dni);

        obj_clsGestorLD.vo_connect();
        str_dniEliminado = obj_clsGestorLD.str_eliminarUsuario(str_dni);
        System.out.println("DEBUG:Eliminado DNI: " + str_dniEliminado);
        obj_clsGestorLD.vo_disconnect();
    }

    /**
     * Modifica los datos de un usuario.
     * 
     * @param str_dniViejo DNI antiguo del usuario.
     * @param str_dniNuevo Nuevo DNI del usuario.
     * @param str_Nombre Nuevo nombre del usuario.
     * @param str_apellido Nuevo apellido del usuario.
     * @param str_contrasena Nueva contrasena del usuario.
     * @param en_rolUsuario Nuevo rol del usuario.
     */
    public void vo_modificarPersona(String str_dniViejo, String str_dniNuevo, 
        String str_Nombre, String str_apellido,
        String str_contrasena, en_Rol en_rolUsuario)
    {
        clsUsuario obj_personaModificada;
        String str_dniModificado;
        clsUsuario obj_usuario = map_listaUsuarios.get(str_dniViejo);
        obj_personaModificada = obj_usuario;
        obj_personaModificada.setDNI(str_dniNuevo);
        obj_personaModificada.setNombre(str_Nombre);
        obj_personaModificada.setApellido(str_apellido);
        obj_personaModificada.setContrasena(str_contrasena);
        obj_personaModificada.setRol(en_rolUsuario);

        obj_clsGestorLD.vo_connect();
        str_dniModificado = obj_clsGestorLD.str_updateUsuario(str_dniViejo, 
            str_dniNuevo, str_Nombre, str_apellido,
            str_contrasena, en_rolUsuario);
        System.out.println("DEBUG:Modificado DNI: " + str_dniModificado);
        obj_clsGestorLD.vo_disconnect();
    }

    /**
     * Crea un nuevo objeto Gimnasio y lo agrega a la lista de gimnasios.
     *
     * @param int_aforo El aforo del gimnasio.
     * @param int_maquinas El numero de maquinas en el gimnasio.
     * @param str_gimnasio El nombre del gimnasio.
     */
    public void vo_gimnasioNuevo(int int_aforo, int int_maquinas, 
        String str_gimnasio)
    {
        clsGimnasio obj_gimansioNuevo;
        String str_id;
        obj_gimansioNuevo = new clsGimnasio(int_aforo, int_maquinas, 
            str_gimnasio);
        set_listaGimnasios.add(obj_gimansioNuevo);

        this.obj_clsGestorLD.vo_connect();
        str_id = obj_clsGestorLD.str_insertarGimnasios(int_aforo, int_maquinas, 
            str_gimnasio);
        System.out.println("DEBUG:Nuevo Gimnasio: " + str_id);
        this.obj_clsGestorLD.vo_disconnect();
    }

    /**
     * Crea un nuevo objeto Txoko (un tipo de club social) y lo agrega a la 
     * lista de Txokos.
     *
     * @param str_presidente El nombre del presidente del Txoko.
     * @param str_txoko El nombre del Txoko.
     * @param int_mesas El numero de mesas en el Txoko.
     */
    public void vo_txokoNuevo(String str_presidente, String str_txoko, 
        int int_mesas)
    {
        clsTxoko obj_TxokoNuevo;
        String str_id;
        Boolean bln_compr;
        bln_compr = false;
        obj_TxokoNuevo = new clsTxoko(str_presidente, str_txoko, int_mesas);
        for (clsTxoko clsAxoko : arrl_listaTxokos)
        {
            if (obj_TxokoNuevo.getStr_txoko().equals(clsAxoko.getStr_txoko()))
            {
                bln_compr = true;
            }
        }
        if (bln_compr == false)
        {
            arrl_listaTxokos.add(obj_TxokoNuevo);

            this.obj_clsGestorLD.vo_connect();
            str_id = obj_clsGestorLD.str_insertarTxokos(str_presidente, 
                str_txoko, int_mesas);
            System.out.println("DEBUG:Nuevo txoko: " + str_id);
            this.obj_clsGestorLD.vo_disconnect();
        }

    }

    /**
     * Crea una nueva piscina y la agrega al sistema.
     * 
     * @param str_Piscina El nombre de la nueva piscina.
     * @param float_temperatura La temperatura de la nueva piscina.
     * @param int_tamano El tamaño de la nueva piscina.
     * @param int_calles El numero de calles de la nueva piscina.
     */
    public void vo_piscinaNueva(String str_Piscina, float float_temperatura, 
        int int_tamano, int int_calles)
    {
        String str_id;
        clsPiscina obj_Piscinanueva;

        obj_Piscinanueva = new clsPiscina(str_Piscina, float_temperatura, 
            int_tamano, int_calles);
        set_listaPiscinas.add(obj_Piscinanueva);

        this.obj_clsGestorLD.vo_connect();
        str_id = obj_clsGestorLD.str_insertarPiscinas(str_Piscina, 
            float_temperatura, int_tamano, int_calles);
        System.out.println("DEBUG:Nueva Piscina: " + str_id);
        this.obj_clsGestorLD.vo_disconnect();
    }

    /**
     * Modifica los detalles de un gimnasio existente en el sistema.
     * 
     * @param int_aforo_nuevo El nuevo aforo del gimnasio.
     * @param int_maquinas_nuevo El nuevo numero de maquinas del gimnasio.
     * @param str_gimnasio_nuevo El nuevo nombre del gimnasio.
     * @param str_gimnasio_viejo El nombre del gimnasio que se desea modificar
     * 
     */
    public void vo_modificarGimnasio(int int_aforo_nuevo, 
        int int_maquinas_nuevo, String str_gimnasio_nuevo,
        String str_gimnasio_viejo)
    {
        clsGimnasio obj_gimansioModificado;
        obj_gimansioModificado = null;
        String str_GimModificado;
        for (clsGimnasio obj_auxGimansio : this.set_listaGimnasios)
        {
            if (obj_auxGimansio.getStr_gimnasio().equals(str_gimnasio_viejo))
            {
                obj_gimansioModificado = obj_auxGimansio;
                obj_gimansioModificado.setInt_aforo(int_aforo_nuevo);
                obj_gimansioModificado.setInt_maquinas(int_maquinas_nuevo);
                obj_gimansioModificado.setStr_gimnasio(str_gimnasio_nuevo);
            }
        }
        obj_clsGestorLD.vo_connect();
        str_GimModificado = obj_clsGestorLD.str_updateGimnasio(int_aforo_nuevo, 
            int_maquinas_nuevo, str_gimnasio_nuevo,
            str_gimnasio_viejo);
        System.out.println("DEBUG:Modificado Gym: " + str_GimModificado);
        obj_clsGestorLD.vo_disconnect();
    }

    /**
     * Modifica los detalles de un txoko existente en el sistema.
     * 
     * @param str_presidente_nuevo El nuevo presidente del txoko.
     * @param str_txoko_nuevo El nuevo nombre del txoko.
     * @param int_mesas_nuevo El nuevo numero de mesas del txoko.
     * @param str_txoko_viejo El nombre del txoko que se desea modificar.
     */
    public void vo_modificarTxoko(String str_presidente_nuevo, 
        String str_txoko_nuevo, int int_mesas_nuevo,
        String str_txoko_viejo)
    {
        clsTxoko obj_txokoModificado;
        String str_txokoModificado;
        for (clsTxoko obj_auxTxoko : this.arrl_listaTxokos)
        {
            if (obj_auxTxoko.getStr_txoko().equals(str_txoko_viejo))
            {
                obj_txokoModificado = obj_auxTxoko;
                obj_txokoModificado.setStr_presidente(str_presidente_nuevo);
                obj_txokoModificado.setStr_txoko(str_txoko_nuevo);
                obj_txokoModificado.setInt_mesas(int_mesas_nuevo);

                obj_clsGestorLD.vo_connect();
                str_txokoModificado = obj_clsGestorLD.str_updateTxoko(
                        str_presidente_nuevo, str_txoko_nuevo,
                        int_mesas_nuevo, str_txoko_viejo);
                System.out.println("DEBUG:Modificado Txoko: " + 
                    str_txokoModificado);
                obj_clsGestorLD.vo_disconnect();
            }
        }
    }

    /**
     * Modifica los detalles de una piscina existente en el sistema y en la bd.
     * 
     * @param str_piscina_nueva El nuevo nombre de la piscina.
     * @param float_temperatura_nueva La nueva temperatura de la piscina.
     * @param int_tamano_nuevo El nuevo tamaño de la piscina.
     * @param int_calles_nuevas El nuevo numero de calles de la piscina.
     * @param str_Piscina_vieja El nombre de la piscina que se desea modificar.
     */
    public void vo_modificarPiscina(String str_piscina_nueva, 
        float float_temperatura_nueva, int int_tamano_nuevo,
        int int_calles_nuevas, String str_Piscina_vieja)
    {
        clsPiscina obj_piscinaModificada;
        String str_PiscinaModificada;
        obj_piscinaModificada = null;
        for (clsPiscina obj_auxPiscina : this.set_listaPiscinas)
        {
            if (obj_auxPiscina.getStr_piscina().equals(str_Piscina_vieja))
            {
                obj_piscinaModificada = obj_auxPiscina;
                obj_piscinaModificada.setStr_piscina(str_piscina_nueva);
                obj_piscinaModificada.setFloat_temperatura
                    (float_temperatura_nueva);
                obj_piscinaModificada.setInt_tamano(int_tamano_nuevo);
                obj_piscinaModificada.setInt_calles(int_calles_nuevas);

                obj_clsGestorLD.vo_connect();
                str_PiscinaModificada = obj_clsGestorLD.str_updatePiscina(
                    str_piscina_nueva, float_temperatura_nueva,
                    int_tamano_nuevo, int_calles_nuevas, str_Piscina_vieja);
                System.out.println("DEBUG:Modificado Piscina: " + 
                    str_PiscinaModificada);
                obj_clsGestorLD.vo_disconnect();
            }
        }
    }

    /**
     * Elimina un gimnasio del sistema.
     * 
     * @param str_Gimnasio El nombre del gimnasio que se desea eliminar.
     */
    public void vo_eliminarGimnasio(String str_Gimnasio)
    {
        clsGimnasio obj_gimnasioEliminado;
        String str_gimnasioEliminado;
        obj_gimnasioEliminado = null;
        for (clsGimnasio obj_auxGimnasio : this.set_listaGimnasios)
        {
            if (obj_auxGimnasio.getStr_gimnasio().equals(str_Gimnasio))
            {
                obj_gimnasioEliminado = obj_auxGimnasio;
            }
        }
        this.set_listaGimnasios.remove(obj_gimnasioEliminado);

        obj_clsGestorLD.vo_connect();
        str_gimnasioEliminado = obj_clsGestorLD.str_eliminarGimnasio
            (str_Gimnasio);
        System.out.println("DEBUG:Eliminado Txoko: " + str_gimnasioEliminado);
        obj_clsGestorLD.vo_disconnect();
    }

    /**
     * Elimina un txoko del sistema.
     * 
     * @param str_txoko El nombre del txoko que se desea eliminar.
     */
    public void vo_eliminarTxoko(String str_txoko)
    {
        clsTxoko obj_txokoEliminado;
        String str_txokoEliminado;
        obj_txokoEliminado = null;
        for (clsTxoko obj_auxTxoko : this.arrl_listaTxokos)
        {
            if (obj_auxTxoko.getStr_txoko().equals(str_txoko))
            {
                obj_txokoEliminado = obj_auxTxoko;
            }
        }
        this.arrl_listaTxokos.remove(obj_txokoEliminado);

        obj_clsGestorLD.vo_connect();
        str_txokoEliminado = obj_clsGestorLD.str_eliminarTxoko(str_txoko);
        System.out.println("DEBUG:Eliminado Txoko: " + str_txokoEliminado);
        obj_clsGestorLD.vo_disconnect();
    }

    /**
     * Elimina una piscina del sistema.
     * 
     * @param str_piscina El nombre de la piscina que se desea eliminar.
     */
    public void vo_eliminarPiscina(String str_piscina)
    {
        clsPiscina obj_piscinaEliminada;
        String str_piscinaEliminada;
        obj_piscinaEliminada = null;
        for (clsPiscina obj_auxPiscina : this.set_listaPiscinas)
        {
            if (obj_auxPiscina.getStr_piscina().equals(str_piscina))
            {
                obj_piscinaEliminada = obj_auxPiscina;
            }
        }
        this.set_listaPiscinas.remove(obj_piscinaEliminada);

        obj_clsGestorLD.vo_connect();
        str_piscinaEliminada = obj_clsGestorLD.str_eliminarPiscina(str_piscina);
        System.out.println("DEBUG:Eliminado Piscina: " + str_piscinaEliminada);
        obj_clsGestorLD.vo_disconnect();
    }

    /**
     * Devuelve una propiedad especifica del objeto.
     * 
     * @param propiedad El nombre de la propiedad que se desea obtener.
     * @return El valor de la propiedad.
     * @throws UnsupportedOperationException Si el metodo no esta implementado.
     */
    @Override
    public Object getProperty(String propiedad)
    {
        throw new UnsupportedOperationException
            ("Eso no esta programado");
    }

    /**
     * Devuelve el rol del usuario.
     * 
     * @return El rol del usuario.
     * @throws UnsupportedOperationException Si el metodo no esta implementado.
     */
    @Override
    public en_Rol get_en_Rol()
    {
        throw new UnsupportedOperationException
            ("Eso no esta programado");
    }

}
