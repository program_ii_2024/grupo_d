package LN;

/**
 * Esta clase representa un establecimiento en el sistema. Es una clase
 * abstracta que proporciona una base para otros tipos de establecimientos, como
 * piscinas, gimnasios o txokos.
 */

public abstract class clsEstablecimiento
{
    /**
     * Longitud geografica de una ubicacion.
     */
    private double dbl_longitud;

    /**
     * Latitud geografica de una ubicacion.
     */
    private double dbl_latitud;

    /**
     * Obtiene la longitud asociada a al establecimiento.
     *
     * @return El valor de la longitud asociado al establecimiento.
     */

    public double getDbl_longitud()
    {
        return dbl_longitud;
    }

    /**
     * Obtiene la latitud asociada a al establecimiento.
     *
     * @return El valor de la latitud asociado al establecimiento.
     */

    public double getDbl_latitud()
    {
        return dbl_latitud;
    }

    /**
     * Establece la longitud asociada a al establecimiento.
     *
     * @param dbl_longitud El valor de la longitud.
     */

    public void setDbl_longitud(double dbl_longitud)
    {
        this.dbl_longitud = dbl_longitud;
    }

    /**
     * Establece la latitud asociada a al establecimiento.
     *
     * @param dbl_latitud El valor de la latitud.
     */

    public void setDbl_latitud(double dbl_latitud)
    {
        this.dbl_latitud = dbl_latitud;
    }
}