package LN;

import COMUN.clsConstantes.en_Dia;
import COMUN.clsConstantes.en_Turno;

/**
 * Representa una reserva en un establecimiento.
 */

public class clsReserva
{
    private String str_dniReserva;
    private String str_nombreEstablecimiento;
    private en_Dia en_dia;
    private en_Turno en_turno;

    /**
     * Constructor de la clase clsReserva
     * 
     * @param str_dniNuevo Dni de la persona que reserva.
     * @param str_nombreEstablNuevo Nombre del establecimiento reservado.
     * @param en_diaNuevo Dia de la reserva.
     * @param en_turnoNuevo Turno de la reserva.
     */

    public clsReserva(String str_dniNuevo, String str_nombreEstablNuevo, 
        en_Dia en_diaNuevo, en_Turno en_turnoNuevo)
    {
        this.str_dniReserva = str_dniNuevo;
        this.str_nombreEstablecimiento = str_nombreEstablNuevo;
        this.en_dia = en_diaNuevo;
        this.en_turno = en_turnoNuevo;
    }

    /**
     * Obtiene el nombre del establecimiento.
     * 
     * @return El nombre del establecimiento.
     */

    public String getStr_nombreEstablecimiento()
    {
        return str_nombreEstablecimiento;
    }

    /**
     * Obtiene el DNI asociado a la reserva.
     * 
     * @return El DNI asociado a la reserva.
     */

    public String getStr_dniReserva()
    {
        return str_dniReserva;
    }

    /**
     * Obtiene el dia de la reserva.
     * 
     * @return El dia de la reserva.
     */

    public en_Dia getEn_dia()
    {
        return en_dia;
    }

    /**
     * Obtiene el turno de la reserva.
     * 
     * @return El turno de la reserva.
     */

    public en_Turno getEn_turno()
    {
        return en_turno;
    }

    /**
     * Establece el nombre del establecimiento.
     * 
     * @param str_nombreEstablecimiento El nombre del establecimiento.
     */

    public void setStr_nombreEstablecimiento(String str_nombreEstablecimiento)
    {
        this.str_nombreEstablecimiento = str_nombreEstablecimiento;
    }

    /**
     * Establece el DNI asociado a la reserva.
     * 
     * @param str_dniReserva El DNI asociado a la reserva.
     */

    public void setStr_dniReserva(String str_dniReserva)
    {
        this.str_dniReserva = str_dniReserva;
    }

    /**
     * Establece el dia de la reserva.
     * 
     * @param en_dia El dia de la reserva.
     */

    public void seten_dia(en_Dia en_dia)
    {
        this.en_dia = en_dia;
    }

    /**
     * Establece el turno de la reserva.
     * 
     * @param en_turno El turno de la reserva.
     */

    public void setEn_turno(en_Turno en_turno)
    {
        this.en_turno = en_turno;
    }

}
