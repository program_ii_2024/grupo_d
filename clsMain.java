import LP.clsMenuLP;

/**
 * Esta clase contiene el metodo principal para ejecutar el programa.
 */

public class clsMain
{

    /**
     * Metodo principal que inicia la aplicacion. Crea un objeto de tipo 
     * clsMenuLP y llama al metodo vo_menuPrincipal() para mostrar el menu 
     * principal.
     * 
     * @param args Argumentos de linea de comandos (no utilizado en este caso).
     */

    public static void main(String[] args)
    {
        clsMenuLP obj_menu;
        obj_menu = new clsMenuLP();
        obj_menu.vo_menuPrincipal();
    }
}