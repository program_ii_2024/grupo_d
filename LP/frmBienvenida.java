package LP;

import javax.swing.ImageIcon;
import java.awt.*;
import static java.awt.Font.BOLD;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * La clase frmBienvenida extiende JFrame y representa una ventana de bienvenida
 * con una animación de texto.
 */
public class frmBienvenida extends JFrame
{
    /** Imagen de la bienvenida */
    private Image img_imgIcon;

    /** Coordenada X del primer texto */
    int int_x = 0;
    /** Coordenada Y del primer texto */
    int int_y = 100;
    /** Coordenada X del segundo texto */
    int int_a = 400;
    /** Coordenada Y del segundo texto */
    int int_b = 200;

    /**
     * Constructor de la clase frmBienvenida. Inicializa la ventana, establece 
     * su ícono y dimensiones, y añade la animación de texto.
     */
    public frmBienvenida()
    {
        img_imgIcon = new ImageIcon(getClass().getResource
            ("/lp/imagen/love.jpg")).getImage();
        setIconImage(img_imgIcon);
        setBounds(600, 180, 820, 500);
        setBackground(new Color(115, 198, 182));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        add(new Text_Animation());
        setLocationRelativeTo(null);
    }

    /**
     * La clase interna Text_Animation extiende JPanel y se encarga de pintar y
     * animar el texto en la ventana.
     */
    public class Text_Animation extends JPanel
    {
        /** Coordenada X del primer texto en la animación */
        int int_x = 0;
        /** Coordenada Y del primer texto en la animación */
        int int_y = 100;
        /** Coordenada X del segundo texto en la animación */
        int int_a = 400;
        /** Coordenada Y del segundo texto en la animación */
        int int_b = 200;

        /**
         * Sobrescribe el método paint para dibujar y animar el texto.
         * 
         * @param grp_gp el contexto gráfico
         */
        public void paint(Graphics grp_gp)
        {
            super.paint(grp_gp);
            Graphics2D grp2d_g2d = (Graphics2D) grp_gp;

            setBackground(new Color(115, 198, 182));
            grp2d_g2d.setColor(new Color(152, 159, 175));
            grp2d_g2d.setFont(new Font("BOLD", BOLD, 35));

            grp2d_g2d.drawString("Bienvenidos a", int_x, int_y);
            grp2d_g2d.drawString("RESERVAS S.L", int_a, int_b);
            grp2d_g2d.drawString("Pasen y Reserven", int_x, 300);

            try
            {
                Thread.sleep(150);
                int_x += 20;
                int_a -= 20;

                if (int_x > getWidth())
                {
                    int_x = 0;
                }
                if (int_a < 0)
                {
                    int_a = 500;
                }
                repaint();
            } catch (InterruptedException ex)
            {
                JOptionPane.showMessageDialog(this, ex);
            }
        }
    }
}