package LP;

/**
 * La clase clsHiloInactividad es un hilo que se ejecutará en segundo plano al
 * mismo tiempo que el programa y llevará un control de inactividad.
 *
 * En el momento que el usuario abra la aplicación, controlaremos si introduce
 * algún dato o no por la consola. Tras pasar un tiempo definido en la variable
 * "segundos" nos saldrá un aviso.
 *
 * Dicho aviso se reseteará y pondremos el contador a cero en el momento que el
 * usuario introduzca un dato por consola de nuevo.
 */
public class clsHiloInactividad extends Thread
{

    /**
     * Indica si el hilo está activo o no.
     */
    private boolean bln_hiloON = true;

    /**
     * Contador utilizado para llevar el seguimiento de ciertas operaciones o
     * eventos.
     */
    private int int_contador = 0;

    /**
     * Establece el estado del hilo.
     * 
     * @param bln_Contando el nuevo estado del hilo
     */
    public void sethiloON(boolean bln_Contando)
    {
        this.bln_hiloON = bln_Contando;
    }

    /**
     * Establece el valor del contador.
     * 
     * @param int_contador el nuevo valor del contador
     */
    public void setContandor(int int_contador)
    {
        this.int_contador = int_contador;
    }

    /**
     * Método que sobreescribimos de la clase Thread que "arranca" el hilo.
     */
    @Override
    public void run()
    {
        System.out.println("Abrimos hilo");
        int int_segundos = 60;

        while (true)
        {
            if ((bln_hiloON == true) && (this.int_contador != int_segundos))
            {
                int_contador++;
                if (int_contador == int_segundos)
                {
                    System.out.println();
                    System.out.println
                        ("Tiempo de inactividad agotado, ¿Sigues ahi?");
                }
            }
            try
            {
                sleep(1000);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
