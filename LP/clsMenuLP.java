package LP;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

import javax.swing.SwingUtilities;

import COMUN.clsConstantes.en_Rol;
import static COMUN.clsConstantes.STR_DNI;
import static COMUN.clsConstantes.STR_NOMBRE;
import static COMUN.clsConstantes.STR_APELLIDO;
import static COMUN.clsConstantes.STR_TXOKO;
import static COMUN.clsConstantes.STR_MESAS;
import static COMUN.clsConstantes.STR_PRESIDENTE_TXOKO;
import static COMUN.clsConstantes.STR_GIMNASIO;
import static COMUN.clsConstantes.STR_AFORO_GIMNASIO;
import static COMUN.clsConstantes.STR_MAQUINAS_GIMNASIO;
import static COMUN.clsConstantes.STR_PISCINA;
import static COMUN.clsConstantes.STR_TAMANO_PICINA;
import static COMUN.clsConstantes.STR_CALLES_PISCINA;
import Excepciones.clsDniErroneo;
import COMUN.itfProperty;
import LN.clsGestorLN;

/**
 * La clase clsMenuLP representa el menu de la aplicacion.
 */

public class clsMenuLP
{
    /**
     * Gestor de lógica de negocio asociado a la ventana.
     */
    private clsGestorLN obj_gestor;

    /**
     * Objeto que representa un hilo de inactividad para controlar ciertas 
     * acciones cuando no hay interacción del usuario.
     */
    public clsHiloInactividad obj_HiloInactividad;

    /**
     * Constructor de la clase clsMenuLP. Inicializa el objeto gestor para 
     * manejar las operaciones relacionadas con los txokos y tambien el hilo que 
     * comprobará la inactividad del usuario.
     */
    public clsMenuLP()
    {
        obj_gestor = new clsGestorLN();
        obj_HiloInactividad = new clsHiloInactividad();
        obj_HiloInactividad.setDaemon(true);
        obj_HiloInactividad.start();
    }

    /**
     * Muestra el menu principal de la aplicacion. Permite al usuario 
     * registrarse, iniciar sesion o salir del programa.
     */

    public void vo_menuPrincipal()
    {
        vo_menuDescargaBD();
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                frmBienvenida obj_Bienvenida = new frmBienvenida();
                obj_Bienvenida.setVisible(true);
            }
        });

        int int_opcion = 99;
        System.out.println();
        System.out.println("Inicio Programa");

        while (int_opcion != 0)
        {
            System.out.println();
            System.out.println("Inicio de Sesion");
            System.out.println("================================");
            System.out.println("0.-Salir");
            System.out.println("1.- Registrarse");
            System.out.println("2.- Inicio de Sesion");
            System.out.println();

            System.out.print("Introduzca la opcion: ");
            int_opcion = clsUtilidades.leerEntero();
            obj_HiloInactividad.setContandor(0);

            switch (int_opcion)
            {
            case 0:
                System.out.println("Vuelva Pronto....");
                break;
            case 1:
                vo_registrarUsuario();
                break;
            case 2:
                vo_iniciarUsuario();
                break;
            default:
                System.out.println("Opción no valida, vuelva a intertarlo");
            }
        }
        System.out.println("Agur MenuPrincipal");
    }

    /**
     * Registra un nuevo usuario en el sistema. Pide al usuario que ingrese
     * informacion basica, como nombre, apellido, DNI y contraseña, y le asigna 
     * un rol de usuario.
     */

    private void vo_registrarUsuario()
    {
        String str_nombre;
        String str_apellido;
        String str_dni;
        String str_contrasena;
        en_Rol en_rolNuevoUsuario;
        char ch_opcionRol;
        en_rolNuevoUsuario = en_Rol.en_Rol_INVALID;

        System.out.println("Introduce el nombre del nuevo usuario: ");
        str_nombre = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce la apellido del nuevo usuario: ");
        str_apellido = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce el dni del nuevo usuario: ");
        str_dni = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce la contraseña del nuevo usuario: ");
        str_contrasena = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);
        do
        {
            System.out.println(
                "Que permisos quieres en esta cuenta, permisos de solo "
                    + "lectura o permisos de lectura-escritura L|E");
            ch_opcionRol = clsUtilidades.leerCaracter();
            switch (ch_opcionRol)
            {
            case 'l':
            case 'L':
                en_rolNuevoUsuario = en_Rol.en_Rol_READ;
                break;
            case 'e':
            case 'E':
                en_rolNuevoUsuario = en_Rol.en_Rol_READ_WRITE;
                break;
            default:
                System.out.println("Esa no es un tipo de permiso.");
                break;
            }
        } while (en_rolNuevoUsuario == en_Rol.en_Rol_INVALID);
        try
        {
            obj_gestor.vo_usuarioNuevo(str_nombre, str_apellido, str_dni, 
                str_contrasena, en_rolNuevoUsuario);
        } catch (clsDniErroneo e)
        {
            System.out.println(e);
        }

    }

    /**
     * Inicia sesion para un usuario existente en el sistema. Permite al usuario
     * ingresar su DNI y contraseña para iniciar sesion en la aplicacion.
     */
    private void vo_iniciarUsuario()
    {

        String str_dni;
        String str_contrasena;
        itfProperty obj_usuarioInicio;
        frmLogin obj_Login = new frmLogin();

        obj_usuarioInicio = null;
        

        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                obj_Login.setVisible(true);
            }
        });

        synchronized (obj_Login)
        {
            while (!obj_Login.isLoginPressed())
            {
                try
                {
                    obj_Login.wait();
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }
        
        str_dni = obj_Login.getUsuarioDNI();
        str_contrasena = obj_Login.getContrasena();
        obj_HiloInactividad.setContandor(0);

        obj_usuarioInicio = obj_gestor.obj_inicioCuenta(str_dni, str_contrasena);
        if (obj_usuarioInicio != null)
        {
            obj_Login.setVisible(false);
            switch (obj_usuarioInicio.get_en_Rol())
            {
            case en_Rol_READ:
                vo_menuLectura(obj_usuarioInicio);
                break;

            case en_Rol_READ_WRITE:
                vo_menuEscritura(obj_usuarioInicio);
                break;

            case en_Rol_ADMIN:
                vo_menuAdmin(obj_usuarioInicio);
                break;
            case en_Rol_INVALID:
                break;

            default:
                break;
            }
        } else
        {
            System.out.println("...");
            System.out.println("El usuario o la contraseña no coinciden");
            System.out.println("...");
            obj_Login.setVisible(false);

        }
        
    }

    /**
     * Muestra el menu de opciones disponibles para un usuario con permisos de
     * escritura. Este menu permite al usuario realizar operaciones que implican
     * cambios en los datos del sistema.
     * 
     * @param obj_usuarioIniciado El usuario que ha iniciado sesion y tiene 
     * permisos de escritura.
     */
    private void vo_menuEscritura(itfProperty obj_usuarioIniciado)
    {
        int int_opcion = 99;
        do
        {
            System.out.println();
            System.out.println("Menu Escritura");
            System.out.println("================================");
            System.out.println("1.- Reserva de una piscina");
            System.out.println("2.- Reserva de un gimnasio");
            System.out.println("3.- Reserva de un txoko");
            System.out.println("4.- Ver tabla de horarios de los gimnasios");
            System.out.println("5.- Ver tabla de horarios de los txokos");
            System.out.println("6.- Ver tabla de horarios de las piscinas");
            System.out.println("0.- Cerrar sesion");
            System.out.println("Introduce una opcion: ");
            int_opcion = clsUtilidades.leerEntero();
            obj_HiloInactividad.setContandor(0);

            switch (int_opcion)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 0:
                System.out.println("Vuelva Pronto, Saliendo...");
                break;
            default:
                System.out.println("Opción no valida, vuelva a intertarlo");
            }
        } while (int_opcion != 0);
    }

    /**
     * Muestra el menu de opciones disponibles para un usuario con permisos de
     * lectura. Este menu permite al usuario ver informacion pero no realizar
     * cambios en el sistema.
     * 
     * @param obj_usuarioIniciado El usuario que ha iniciado sesion y tiene 
     * permisos de lectura.
     */
    private void vo_menuLectura(itfProperty obj_usuarioIniciado)
    {
        int int_opcion = 99;
        do
        {
            System.out.println();
            System.out.println("Menu Lectura");
            System.out.println("================================");
            System.out.println("1.- Ver tabla de horarios de las piscinas");
            System.out.println("2.- Ver tabla de horarios del los gimnasios");
            System.out.println("3.- Ver tabla de horarios de los txokos");
            System.out.println("0.- Cerrar sesion");
            System.out.println("Introduce una opcion: ");
            int_opcion = clsUtilidades.leerEntero();
            obj_HiloInactividad.setContandor(0);

            switch (int_opcion)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 0:
                System.out.println("Vuelva Pronto, Saliendo...");
                break;
            default:
                System.out.println("Opción no valida, vuelva a intertarlo");
            }
        } while (int_opcion != 0);
    }

    /**
     * Muestra el menu de opciones disponibles para un administrador. Este menu
     * proporciona opciones avanzadas de administracion del sistema.
     * 
     * @param obj_usuarioIniciado El usuario administrador que ha iniciado sesion.
     */
    public void vo_menuAdmin(itfProperty obj_usuarioIniciado)
    {
        int int_opcion = 99;
        do
        {
            System.out.println();
            System.out.println("Menu Admin");
            System.out.println("================================");
            System.out.println("1.- Visualizar datos");
            System.out.println("2.- Dar de alta");
            System.out.println("3.- Dar de baja");
            System.out.println("4.- Modificar datos");
            // System.out.println("5.- Ver horarios");
            // System.out.println("6.- Anular reserva");
            System.out.println("7.- Usar Pantalla");
            System.out.println("0.- Cerrar sesion");
            System.out.println("Introduce una opcion: ");
            int_opcion = clsUtilidades.leerEntero();
            obj_HiloInactividad.setContandor(0);
            switch (int_opcion)
            {
            case 1:
                int int_visual = 99;
                do
                {
                    System.out.println();
                    System.out.println("Menu Visualizar Datos Admin");
                    System.out.println("================================");
                    System.out.println("1.- Visualizar Usuarios");
                    System.out.println("2.- Visualizar Piscina");
                    System.out.println("3.- Visualizar Txoko");
                    System.out.println("4.- Visualizar Txoko Ordenado");
                    System.out.println("5.- Visualizar Gimnasio");
                    System.out.println("0.- Salir de visualizacion");
                    System.out.println("Introduce una opcion: ");
                    int_visual = clsUtilidades.leerEntero();
                    obj_HiloInactividad.setContandor(0);
                    switch (int_visual)
                    {
                    case 1:
                        vo_visualizarUsuarios();
                        break;
                    case 2:
                        vo_visualizarPiscinas();
                        break;
                    case 3:
                        vo_visualizarTxokos();
                        break;
                    case 4:
                        vo_visualizarTxokosOrden();
                        break;
                    case 5:

                        vo_visualizarGimnasios();
                        break;
                    case 0:
                        System.out.println();
                        System.out.println("Saliendo de visualizar...");
                        break;
                    default:
                        System.out.println
                            ("Opción no valida, vuelva a intertarlo");
                    }
                } while (int_visual != 0);
                break;
            case 2:
                int int_alta = 99;
                do
                {
                    System.out.println();
                    System.out.println("Menu Alta Admin");
                    System.out.println("===============================");
                    System.out.println("1.- Alta de un nuevo Usuario");
                    System.out.println("2.- Alta de una nueva Piscina");
                    System.out.println("3.- Alta de un nuevo Txoko");
                    System.out.println("4.- Alta de un nuevo Gimnasio");
                    System.out.println("0.- Salir de altas");
                    System.out.println("Introduce una opcion: ");
                    int_alta = clsUtilidades.leerEntero();
                    obj_HiloInactividad.setContandor(0);
                    switch (int_alta)
                    {
                    case 1:
                        vo_registrarUsuario();
                        break;
                    case 2:
                        vo_menuAltaPiscina();
                        break;
                    case 3:
                        vo_menuAltaTxoko();
                        break;
                    case 4:
                        vo_menuAltaGimnasio();
                        break;
                    case 0:
                        System.out.println();
                        System.out.println("Saliendo de alta...");
                        break;
                    default:
                        System.out.println
                            ("Opción no valida, vuelva a intertarlo");
                    }
                } while (int_alta != 0);
                break;
            case 3:
                int int_baja = 99;
                do
                {
                    System.out.println();
                    System.out.println("Menu Baja Admin");
                    System.out.println("===============================");
                    System.out.println("1.- Baja de un  Usuario");
                    System.out.println("2.- Baja de una  Piscina");
                    System.out.println("3.- Baja de un  Txoko");
                    System.out.println("4.- Baja de un  Gimnasio");
                    System.out.println("0.- Salir de bajas");
                    System.out.println("Introduce una opcion: ");
                    int_baja = clsUtilidades.leerEntero();
                    obj_HiloInactividad.setContandor(0);
                    switch (int_baja)
                    {
                    case 1:
                        vo_menuBajaPersona();
                        break;
                    case 2:
                        vo_menuBajaPiscina();
                        break;
                    case 3:
                        vo_menuBajaTxoko();
                        break;
                    case 4:
                        vo_menuBajaGimnasio();
                        break;
                    case 0:
                        System.out.println();
                        System.out.println("Saliendo de baja...");
                        break;
                    default:
                        System.out.println
                            ("Opción no valida, vuelva a intertarlo");
                    }
                } while (int_baja != 0);
                break;
            case 4:
                int int_modi = 99;
                do
                {
                    System.out.println();
                    System.out.println("Menu Modificar Admin");
                    System.out.println("===============================");
                    System.out.println("1.- Modificar un  Usuario");
                    System.out.println("2.- Modificar una  Piscina");
                    System.out.println("3.- Modificar un  Txoko");
                    System.out.println("4.- Modificar un  Gimnasio");
                    System.out.println("0.- Salir de modificaciones");
                    System.out.println("Introduce una opcion: ");
                    int_modi = clsUtilidades.leerEntero();
                    obj_HiloInactividad.setContandor(0);
                    switch (int_modi)
                    {
                    case 1:
                        vo_menuModificarPersona();
                        break;
                    case 2:
                        vo_menuModificacionPiscina();
                        break;
                    case 3:
                        vo_menuModificacionTxoko();
                        break;
                    case 4:
                        vo_menuModificacionGimnasio();
                        break;
                    case 0:
                        System.out.println();
                        System.out.println("Saliendo de modificar...");
                        break;
                    default:
                        System.out.println
                            ("Opción no valida, vuelva a intertarlo");
                    }
                } while (int_modi != 0);
                break;
            case 5:
                int int_horario = 99;
                do
                {
                    System.out.println();
                    System.out.println("Menu Horario Admin");
                    System.out.println("===============================");
                    System.out.println("1.- Horario Piscinas");
                    System.out.println("2.- Horario Txoko");
                    System.out.println("3.- Horario Gimnasio");
                    System.out.println("0.- Salir de horarios");
                    System.out.println("Introduce una opcion: ");
                    int_horario = clsUtilidades.leerEntero();
                    obj_HiloInactividad.setContandor(0);
                    switch (int_horario)
                    {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:

                    case 0:
                        System.out.println();
                        System.out.println("Saliendo de horario...");
                        break;
                    default:
                        System.out.println
                            ("Opción no valida, vuelva a intertarlo");
                    }
                } while (int_horario != 0);
                break;
            case 6:
                int int_reserva = 99;
                do
                {
                    System.out.println();
                    System.out.println("Menu Anular Reserva Admin");
                    System.out.println("===============================");
                    System.out.println("1.- Reserva Piscinas");
                    System.out.println("2.- Reserva Txoko");
                    System.out.println("3.- Reserva Gimnasio");
                    System.out.println("0.- Salir de horarios");
                    System.out.println("Introduce una opcion: ");
                    int_reserva = clsUtilidades.leerEntero();
                    obj_HiloInactividad.setContandor(0);
                    switch (int_reserva)
                    {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 0:
                        System.out.println();
                        System.out.println("Saliendo de reservas...");
                        break;
                    default:
                        System.out.println
                            ("Opción no valida, vuelva a intertarlo");
                    }
                } while (int_reserva != 0);
                break;
            case 7:

                SwingUtilities.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        frmMenu obj_Menu = new frmMenu(obj_gestor);
                        obj_Menu.setVisible(true);
                    }
                });
                break;
            case 0:
                System.out.println();
                System.out.println("Vuelva Pronto, Saliendo...");
                break;
            default:
                System.out.println("Opción no valida, vuelva a intertarlo");
            }
        } while (int_opcion != 0);
    }

    /**
     * Descarga los datos de la Base de Datos.
     *
     * Dice si la operacion se ha relizado con exito o no
     */
    private void vo_menuDescargaBD()
    {
        if (!obj_gestor.bln_descargarUsuariosBD())
        {
            System.out.println("Descarga de datos de usuarios correcta");
        } else
        {
            System.out.println("Fallo en la descarga de datos de usuarios");
        }

        if (!obj_gestor.bln_descargarTxokosBD())
        {
            System.out.println("Descarga de datos de txokos correcta");
        } else
        {
            System.out.println("Fallo en la descarga de datos de txokos");
        }

        if (!obj_gestor.bln_descargarGimnasiosBD())
        {
            System.out.println("Descarga de datos de gimnasios correcta");
        } else
        {
            System.out.println("Fallo en la descarga de datos de los" 
                + "gimnasios");
        }
        if (!obj_gestor.bln_descargarPiscinasBD())
        {
            System.out.println("Descarga de datos de piscinas correcta");
        } else
        {
            System.out.println("Fallo en la descarga de datos de los" 
                + "piscinas");
        }
    }

    /**
     * Elimina una piscina.
     * 
     * Nombre de la piscina que se desea eliminar y la manda a la clase gestor.
     */
    private void vo_menuBajaPiscina()
    {
        String str_Piscina_aeliminar;
        vo_visualizarPiscinas();
        System.out.println("Introduzca nombre de la piscina que se desea " 
            + "eliminar ");

        str_Piscina_aeliminar = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        obj_gestor.vo_eliminarPiscina(str_Piscina_aeliminar);
    }

    /**
     * Elimina un txoko. Pide los siguientes datos y los manda a la clase gestor:
     * Nombre del txoko que se desea eliminar.
     */
    private void vo_menuBajaTxoko()
    {
        String str_txoko_aeliminar;
        vo_visualizarTxokos();

        System.out.println("Introduzca nombre del txoko que se desea " 
            + "eliminar ");

        str_txoko_aeliminar = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        obj_gestor.vo_eliminarTxoko(str_txoko_aeliminar);
    }

    /**
     * Elimina un gimnasio. Pide los siguientes datos y los manda a la clase 
     * gestor: Nombre del gimnasio que se desea eliminar.
     */
    private void vo_menuBajaGimnasio()
    {
        String str_Gimnasio_aeliminar;
        vo_visualizarGimnasios();

        System.out.println("Introduzca nombre del gimnasio que se desea " 
            + "eliminar ");

        str_Gimnasio_aeliminar = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        obj_gestor.vo_eliminarGimnasio(str_Gimnasio_aeliminar);

    }

    /**
     * Modifica los atributos de una piscina existente.
     *
     * Pide los siguientes datos y los manda a la clase gestor: El nuevo nombre 
     * de la piscina. La nueva temperatura de la piscina. El nuevo tamaño de la
     * piscina. La cantidad de calles nuevas en la piscina. El nombre de la 
     * piscina que se desea modificar.
     */
    private void vo_menuModificacionPiscina()
    {
        String str_Piscina_nuevo;
        float float_temperatura_nuevo;
        int int_tamaño_nuevo;
        int int_calles_nuevo;
        String str_Piscina_viejo;

        vo_visualizarPiscinas();

        System.out.println("Introduzca nombre de la piscina que se desea " 
            + "modificar ");

        str_Piscina_viejo = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce el nombre de la piscina nueva: ");
        str_Piscina_nuevo = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce la temperatura nueva de la piscina ");
        float_temperatura_nuevo = clsUtilidades.leerFloat();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce el tamaño de la piscina nueva: ");
        int_tamaño_nuevo = clsUtilidades.leerEntero();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce cantidad de calles nuevas ");
        int_calles_nuevo = clsUtilidades.leerEntero();
        obj_HiloInactividad.setContandor(0);

        obj_gestor.vo_modificarPiscina(str_Piscina_nuevo, 
            float_temperatura_nuevo, int_tamaño_nuevo, int_calles_nuevo,
            str_Piscina_viejo);
    }

    /**
     * Modifica los datos de un txoko existente.
     * 
     * Pide los siguientes datos y los manda a la clase gestor: El nombre del 
     * nuevo presidente. El nombre del txoko nuevo. La cantidad de mesas nuevas 
     * que tiene el txoko. El nombre del txoko que se desea modificar.
     */
    private void vo_menuModificacionTxoko()
    {
        String str_presidente_nuevo;
        String str_txoko_nuevo;
        int int_mesas_nuevo;
        String str_txoko_viejo;

        vo_visualizarTxokos();

        System.out.println("Introduzca nombre del txoko que se desea " 
            + "modificar ");

        str_txoko_viejo = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce el nombre del txoko nuevo: ");
        str_txoko_nuevo = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce el nombre del nuevo presidente: ");
        str_presidente_nuevo = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce cantidad de mesas nuevas que tiene: ");
        int_mesas_nuevo = clsUtilidades.leerEntero();
        obj_HiloInactividad.setContandor(0);

        obj_gestor.vo_modificarTxoko(str_presidente_nuevo, str_txoko_nuevo, 
            int_mesas_nuevo, str_txoko_viejo);

    }

    /**
     * Clase para modificar un gimnasio.
     * 
     * Pide los siguientes datos y los manda a la clase gestor: El aforo del 
     * nuevo gimnasio. La cantidad de maquinas en el gimnasio. El nombre del 
     * gimnasio. El nombre del antiguo del gimnasio.
     */
    private void vo_menuModificacionGimnasio()
    {
        int int_aforo_nuevo;
        int int_maquinas_nuevo;
        String str_Gimnasio_nuevo;
        String str_Gimnasio_viejo;

        vo_visualizarGimnasios();

        System.out.println("Introduzca nombre del gimnasio que se desea " 
            + "modificar ");

        str_Gimnasio_viejo = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce el nombre del gimnasio nuevo: ");
        str_Gimnasio_nuevo = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce el aforo del nuevo Gimnasio nuevo: ");
        int_aforo_nuevo = clsUtilidades.leerEntero();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce cantidad de maquinas nuevas: ");
        int_maquinas_nuevo = clsUtilidades.leerEntero();
        obj_HiloInactividad.setContandor(0);

        obj_gestor.vo_modificarGimnasio(int_aforo_nuevo, int_maquinas_nuevo, 
            str_Gimnasio_nuevo, str_Gimnasio_viejo);

    }

    /**
     * Este metodo permite dar de alta una nueva piscina. Se solicita al usuario
     * ingresar el nombre, la temperatura, el tamaño y la cantidad de calles de 
     * la piscina.
     *
     */
    private void vo_menuAltaPiscina()
    {
        String str_Piscina;
        float float_temperatura;
        int int_tamaño;
        int int_calles;

        System.out.println("Introduce el nombre de la piscina: ");
        str_Piscina = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce la temperatura de la piscina ");
        float_temperatura = clsUtilidades.leerFloat();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce el tamaño de la piscina: ");
        int_tamaño = clsUtilidades.leerEntero();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce cantidad de calles de la piscina: ");
        int_calles = clsUtilidades.leerEntero();
        obj_HiloInactividad.setContandor(0);

        obj_gestor.vo_piscinaNueva(str_Piscina, float_temperatura, 
            int_tamaño, int_calles);

    }

    /**
     * Este metodo permite dar de alta un nuevo txoko.
     * 
     * Pide los siguientes datos y los manda a la clase gestor: El nombre del
     * presidente del txoko. El nombre del txoko. La cantidad de mesas que tiene 
     * el txoko.
     */
    private void vo_menuAltaTxoko()
    {
        String str_presidente;
        String str_txoko;
        int int_mesas;

        System.out.println("Introduce el nombre del txoko: ");
        str_txoko = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce el nombre del presidente: ");
        str_presidente = clsUtilidades.leerCadena();
        obj_HiloInactividad.setContandor(0);

        System.out.println("Introduce cantidad de mesas que tiene: ");
        int_mesas = clsUtilidades.leerEntero();
        obj_HiloInactividad.setContandor(0);

        obj_gestor.vo_txokoNuevo(str_presidente, str_txoko, int_mesas);
    }

    /**
     * Este metodo se utiliza para dar de alta un nuevo gimnasio. Solicita al
     * usuario ingresar el nombre del gimnasio, el aforo y la cantidad de 
     * maquinas. Luego, llama al metodo obj_gimnasioNuevo del objeto obj_gestor.
     *
     * Pide los siguientes datos y los manda a la clase gestor: 
     * El aforo del nuevo gimnasio. La cantidad de maquinas en el gimnasio. 
     * El nombre del gimnasio.
     */
    private void vo_menuAltaGimnasio()
    {
        int int_aforo;
        int int_maquinas;
        String str_Gimnasio;

        System.out.println("Introduce el nombre del gimnasio: ");
        str_Gimnasio = clsUtilidades.leerCadena();

        System.out.println("Introduce el aforo del nuevo Gimnasio: ");
        int_aforo = clsUtilidades.leerEntero();

        System.out.println("Introduce cantidad de maquinas: ");
        int_maquinas = clsUtilidades.leerEntero();

        obj_gestor.vo_gimnasioNuevo(int_aforo, int_maquinas, str_Gimnasio);
    }

    /**
     * Muestra la lista de usuarios registrados en la aplicacion.
     */
    public void vo_visualizarUsuarios()
    {
        ArrayList<itfProperty> arrl_listausuarios;
        String str_informacion;
        str_informacion = "============Datos de Usuarios:============\n";
        str_informacion += "Nombre--Apellido--DNI\n";
        arrl_listausuarios = obj_gestor.arrl_mostrarUsuarios();
        for (itfProperty obj_aux : arrl_listausuarios)
        {
            str_informacion += (String) ((itfProperty) obj_aux).getProperty
                (STR_NOMBRE) + " -- ";
            str_informacion += (String) ((itfProperty) obj_aux).getProperty
                (STR_APELLIDO) + " -- ";
            str_informacion += (String) ((itfProperty) obj_aux).getProperty
                (STR_DNI) + "\n";
        }
        System.out.println(str_informacion);
    }

    /**
     * Muestra los nombres de las piscinas.
     */
    public void vo_visualizarPiscinas()
    {
        HashSet<itfProperty> set_listaPiscina;
        String str_informacion;
        set_listaPiscina = obj_gestor.set_mostrarPiscinas();
        str_informacion = "============Datos de Piscinas:============\n";
        str_informacion += "Nombre--Tamano--Calles\n";

        for (itfProperty obj_auxpiscina : set_listaPiscina)
        {
            str_informacion += (String) obj_auxpiscina.getProperty
                (STR_PISCINA) + " -- ";
            str_informacion += obj_auxpiscina.getProperty
                (STR_TAMANO_PICINA).toString() + " -- ";
            str_informacion += obj_auxpiscina.getProperty
                (STR_CALLES_PISCINA).toString() + "\n";
        }
        System.out.println(str_informacion);
    }

    /**
     * Muestra los nombres de los txokos.
     */
    public void vo_visualizarTxokos()
    {
        String str_informacion;
        ArrayList<itfProperty> arrl_txoko;
        arrl_txoko = obj_gestor.arrl_mostrarTxokos();
        str_informacion = "============Datos de Txokos:============\n";
        str_informacion += "Nombre--Presidente--NumeroMesas\n";

        for (itfProperty obj_auxTxoko : arrl_txoko)
        {
            str_informacion += (String) obj_auxTxoko.getProperty
                (STR_TXOKO) + " -- ";
            str_informacion += (String) obj_auxTxoko.getProperty
                (STR_PRESIDENTE_TXOKO) + " -- ";
            str_informacion += obj_auxTxoko.getProperty
                (STR_MESAS).toString() + "\n";
        }
        System.out.println(str_informacion);
    }

    /**
     * Muestra los nombres de los txokos.
     */
    public void vo_visualizarTxokosOrden()
    {
        String str_informacion;
        ArrayList<itfProperty> arrl_listaTxoko;
        arrl_listaTxoko = obj_gestor.arrl_mostrarTxokosOrdenados();
        str_informacion = "===========Datos de Txokos Ordenados:===========\n";
        str_informacion += "Nombre--Presidente--NumeroMesas\n";
        for (itfProperty obj_auxTxoko : arrl_listaTxoko)
        {
            str_informacion += (String) obj_auxTxoko.getProperty
                (STR_TXOKO) + " -- ";
            str_informacion += (String) obj_auxTxoko.getProperty
                (STR_PRESIDENTE_TXOKO) + " -- ";
            str_informacion += obj_auxTxoko.getProperty(
                STR_MESAS).toString() + "\n";
        }
        System.out.println(str_informacion);
    }

    /**
     * Muestra los nombres de los gimnasios.
     */
    public void vo_visualizarGimnasios()
    {
        String str_informacion;
        TreeSet<itfProperty> arrl_gimnasio;
        arrl_gimnasio = obj_gestor.set_mostrarGimnasios();
        str_informacion = "===========Datos de Gimnasios:===========\n";
        str_informacion += "Nombre--Aforo--Maquinas\n";
        for (itfProperty obj_auxGimnasio : arrl_gimnasio)
        {
            str_informacion += (String) obj_auxGimnasio.getProperty
                (STR_GIMNASIO) + " -- ";
            str_informacion += obj_auxGimnasio.getProperty
                (STR_AFORO_GIMNASIO).toString() + " -- ";
            str_informacion += obj_auxGimnasio.getProperty
                (STR_MAQUINAS_GIMNASIO).toString() + "\n";

        }
        System.out.println(str_informacion);
    }

    /**
     * Muestra el menu para dar de baja a un usuario. Permite al administrador
     * eliminar un usuario del sistema.
     */
    public void vo_menuBajaPersona()
    {
        String str_dni;
        vo_visualizarUsuarios();
        do
        {
            System.out.println("Introduzca DNI usuario que desea" 
                + "eliminar o 'salir' para terminar");
            str_dni = clsUtilidades.leerCadena();
            obj_HiloInactividad.setContandor(0);
        } while (!obj_gestor.bln_comprobarDni(str_dni) &&  
            !str_dni.equals("salir"));

        if (!str_dni.equalsIgnoreCase("salir"))
        {
            obj_gestor.vo_eliminarPersona(str_dni);
        }
    }

    /**
     * Muestra el menu para modificar los datos de un usuario. Permite al
     * administrador modificar los datos de un usuario existente en el sistema.
     */
    public void vo_menuModificarPersona()
    {
        String str_dniViejo;
        String str_dniNuevo;
        String str_nombreNuevo;
        String str_apellidoNuevo;
        String str_contrasenaNuevo;
        en_Rol en_rolUsuario;
        char ch_opcionRol;
        en_rolUsuario = en_Rol.en_Rol_INVALID;
        vo_visualizarUsuarios();
        do
        {
            System.out.println("Introduzca DNI del usuario que se desea a " 
                + "modificar o 'salir' para terminar");
            str_dniViejo = clsUtilidades.leerCadena();
            obj_HiloInactividad.setContandor(0);
        } while (!obj_gestor.bln_comprobarDni(str_dniViejo) && 
            !str_dniViejo.equals("salir"));

        if (!str_dniViejo.equalsIgnoreCase("salir"))
        {
            System.out.println("Introduzca nuevo DNI");
            str_dniNuevo = clsUtilidades.leerCadena();
            obj_HiloInactividad.setContandor(0);

            System.out.println("Introduzca nuevo nombre");
            str_nombreNuevo = clsUtilidades.leerCadena();
            obj_HiloInactividad.setContandor(0);

            System.out.println("Introduzca nuevo apellido");
            str_apellidoNuevo = clsUtilidades.leerCadena();
            obj_HiloInactividad.setContandor(0);

            System.out.println("Introduzca nueva contraseña");
            str_contrasenaNuevo = clsUtilidades.leerCadena();
            obj_HiloInactividad.setContandor(0);

            do
            {
                System.out.println("Que permisos quieres en esta cuenta," 
                    + "permisos de solo lectura o permisos de "
                    + "lectura-escritura L|E");
                ch_opcionRol = clsUtilidades.leerCaracter();
                obj_HiloInactividad.setContandor(0);
                switch (ch_opcionRol)
                {
                case 'l':
                case 'L':
                    en_rolUsuario = en_Rol.en_Rol_READ;
                    break;
                case 'e':
                case 'E':
                    en_rolUsuario = en_Rol.en_Rol_READ_WRITE;
                    break;
                default:
                    System.out.println("Esa no es un tipo de permiso.");
                    break;
                }
            } while (en_rolUsuario == en_Rol.en_Rol_INVALID);

            obj_gestor.vo_modificarPersona(str_dniViejo, str_dniNuevo, 
                str_nombreNuevo, str_apellidoNuevo,
                str_contrasenaNuevo, en_rolUsuario);
        }
    }
}
