package LP;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * La clase frmLogin extiende JFrame e implementa ActionListener y KeyListener
 * para proporcionar una interfaz de usuario de inicio de sesión.
 */
public class frmLogin extends JFrame implements ActionListener, KeyListener
{
    /** Panel para logearse */
    private JPanel panel_Login;

    /** Campo de texto para el nombre de usuario */
    private JTextField txt_Usuario;
    /** Campo de texto para la contraseña en formato de contraseña */
    private JPasswordField pasw_Contrasena;
    /** Campo de texto para la contraseña en formato de texto visible */
    private JTextField txt_Contrasenavisible;
    /** Ícono de la ventana */
    private Image img_Icon;

    /** Etiqueta para el campo de nombre de usuario */
    private JLabel lbl_Usuario;
    /** Etiqueta para el campo de contraseña */
    private JLabel lbl_Contrasena;
    /** Etiqueta para mostrar si se debe ver o no la contraseña */
    private JLabel lbl_ContrasenaVisible;

    /** Botón para iniciar sesión */
    private JButton btn_Aceptar;
    /** Botón para mostrar u ocultar la contraseña */
    private JButton btn_Ver;

    /** Nombre de usuario ingresado */
    private String str_Usuario;
    /** Contraseña ingresada */
    private String str_Contrasena;

    /** Indica si el botón de inicio de sesión ha sido presionado */
    private boolean bln_loginPressed;

    /** Indica si se debe ocultar la contraseña */
    private boolean bln_ocultar;

    /**
     * Constructor de la clase frmLogin. Inicializa la interfaz gráfica de 
     * usuario para el inicio de sesión.
     */
    public frmLogin()
    {
        img_Icon = new ImageIcon(getClass().getResource
            ("/lp/imagen/love.jpg")).getImage();
        setIconImage(img_Icon);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setBounds(500, 180, 500, 500);
        setTitle("JMenu");

        bln_loginPressed = false;
        bln_ocultar = false;

        panel_Login = new JPanel();
        panel_Login.setBounds(10, 10, 100, 100);
        panel_Login.setBackground(new Color(64, 54, 58));
        setContentPane(panel_Login);
        panel_Login.setLayout(null);

        txt_Usuario = new JTextField();
        txt_Usuario.setBounds(170, 50, 140, 20);
        txt_Usuario.addKeyListener(this);
        panel_Login.add(txt_Usuario);

        lbl_Usuario = new JLabel("Usuario");
        lbl_Usuario.setBounds(170, 35, 140, 14);
        lbl_Usuario.setForeground(new Color(249, 225, 234));
        panel_Login.add(lbl_Usuario);

        pasw_Contrasena = new JPasswordField();
        pasw_Contrasena.setBounds(170, 90, 140, 20);
        pasw_Contrasena.addKeyListener(this);
        panel_Login.add(pasw_Contrasena);

        lbl_Contrasena = new JLabel("Contrasena");
        lbl_Contrasena.setBounds(170, 75, 140, 14);
        lbl_Contrasena.setForeground(new Color(249, 225, 234));
        panel_Login.add(lbl_Contrasena);

        txt_Contrasenavisible = new JTextField();
        txt_Contrasenavisible.setBounds(170, 140, 140, 20);
        txt_Contrasenavisible.setEnabled(false);
        panel_Login.add(txt_Contrasenavisible);

        lbl_ContrasenaVisible = new JLabel("Ver contrasena");
        lbl_ContrasenaVisible.setBounds(170, 125, 140, 14);
        lbl_ContrasenaVisible.setForeground(new Color(249, 225, 234));
        panel_Login.add(lbl_ContrasenaVisible);

        btn_Ver = new JButton("Ver");
        btn_Ver.addActionListener(this);
        btn_Ver.setActionCommand("V");
        btn_Ver.setBounds(170, 200, 140, 30);
        panel_Login.add(btn_Ver);

        btn_Aceptar = new JButton("Aceptar");
        btn_Aceptar.addActionListener(this);
        btn_Aceptar.addKeyListener(this);
        btn_Aceptar.setActionCommand("A");
        btn_Aceptar.setBounds(170, 170, 140, 30);
        panel_Login.add(btn_Aceptar);
    }

    /**
     * Obtiene el nombre de usuario ingresado.
     * 
     * @return el nombre de usuario
     */
    public String getUsuarioDNI()
    {
        return str_Usuario;
    }

    /**
     * Obtiene la contraseña ingresada.
     *
     * @return la contraseña
     */
    public String getContrasena()
    {
        return str_Contrasena;
    }

    /**
     * Verifica si el botón de inicio de sesión ha sido presionado.
     * 
     * @return verdadero si el botón de inicio de sesión ha sido presionado, falso
     * en caso contrario
     */
    public boolean isLoginPressed()
    {
        return bln_loginPressed;
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        char[] ch_passwordChars;

        if (e.getKeyCode() == KeyEvent.VK_ENTER)
        {
            str_Usuario = txt_Usuario.getText();

            ch_passwordChars = pasw_Contrasena.getPassword();
            str_Contrasena = new String(ch_passwordChars);

            bln_loginPressed = true;
            synchronized (frmLogin.this)
            {
                frmLogin.this.notify();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        char[] ch_passwordChars;
        String str_password;
        switch (e.getActionCommand())
        {
        case "A":
            str_Usuario = txt_Usuario.getText();

            ch_passwordChars = pasw_Contrasena.getPassword();
            str_Contrasena = new String(ch_passwordChars);

            bln_loginPressed = true;
            synchronized (frmLogin.this)
            {
                frmLogin.this.notify();
            }

        case "V":
            if (bln_ocultar == false)
            {
                ch_passwordChars = pasw_Contrasena.getPassword();
                str_password = new String(ch_passwordChars);
                txt_Contrasenavisible.setText(str_password);
                bln_ocultar = true;
            } else
            {
                txt_Contrasenavisible.setText("");
                bln_ocultar = false;
            }

            break;
        }
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
    }
}