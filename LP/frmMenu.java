package LP;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static COMUN.clsConstantes.STR_TXOKO;
import static COMUN.clsConstantes.STR_MESAS;
import static COMUN.clsConstantes.STR_PRESIDENTE_TXOKO;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import COMUN.itfProperty;
import LN.clsGestorLN;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * La clase frmMenu representa la ventana principal de la aplicación. Esta
 * ventana proporciona opciones para la gestión de datos de los txokos.
 */
public class frmMenu extends JFrame implements ActionListener
{
    /**
     * Panel para el manejo de datos relacionados con los txokos.
     */
    private JPanel panel_panelManejo;

    /**
     * Panel que contiene una tabla de txokos.
     */
    private PanelTablaTxoko panel_panelTabla;

    /**
     * Tabla que muestra los datos de los txokos.
     */
    private JTable tbl_Txokos;

    /**
     * Campo de texto para ingresar el nombre del txoko.
     */
    private JTextField txt_NombreTxoko;

    /**
     * Campo de texto para ingresar el nombre del presidente del txoko.
     */
    private JTextField txt_Presidente;

    /**
     * Campo de texto para ingresar el número de mesas del txoko.
     */
    private JTextField txt_NumMesas;

    /**
     * Campo de texto para ingresar el nombre antiguo del txoko en caso de
     * modificación.
     */
    private JTextField txt_NombreViejoTxoko;

    /**
     * Etiqueta que indica el propósito del campo de texto txt_NombreTxoko.
     */
    private JLabel lbl_NombreTxoko;

    /**
     * Etiqueta que indica el propósito del campo de texto txt_Presidente.
     */
    private JLabel lbl_Presidente;

    /**
     * Etiqueta que indica el propósito del campo de texto txt_NumMesas.
     */
    private JLabel lbl_NumMesas;

    /**
     * Etiqueta que indica el propósito del campo de texto txt_NombreViejoTxoko.
     */
    private JLabel lbl_NombreViejoTxoko;

    /**
     * Botón para aceptar operaciones o acciones.
     */
    private JButton btn_Aceptar;

    /**
     * Botón para realizar el alta de un nuevo txoko.
     */
    private JButton btn_Alta;

    /**
     * Botón para realizar la baja de un txoko existente.
     */
    private JButton btn_Baja;

    /**
     * Botón para realizar la modificación de un txoko existente.
     */
    private JButton btn_Modi;

    /**
     * Icono de la aplicación.
     */
    private Image img_Icon;

    /**
     * Gestor de lógica de negocio asociado a la ventana.
     */
    private clsGestorLN obj_gestor;

    /**
     * Datos de la tabla de txokos.
     */
    private Object[][] obj_data;

    /**
     * Lista de propiedades de los txokos.
     */
    private ArrayList<itfProperty> arl_Txokos;

    /**
     * Modelo de tabla predeterminado para la tabla de txokos.
     */
    private DefaultTableModel mod_model;

    /**
     * Caso o situación actual.
     */
    private int int_caso;

    /**
     * Barra de menú de la aplicación.
     */
    private JMenuBar mnb_menuBar;

    /**
     * Menú principal de la barra de menú.
     */
    private JMenu menu_menu1;

    /**
     * Segundo menú de la barra de menú.
     */
    private JMenu menu_menu2;

    /**
     * Tercer menú de la barra de menú.
     */
    private JMenu menu_menu3;

    /**
     * Elemento de menú para una acción específica en el segundo menú.
     */
    private JMenuItem menui_menuItem21;

    /**
     * Elemento de menú para una acción específica en el segundo menú.
     */
    private JMenuItem menui_menuItem22;

    /**
     * Elemento de menú para una acción específica en el tercer menú.
     */
    private JMenuItem menui_menuItem31;

    /**
     * Grupo de botones asociados para opciones mutuamente excluyentes.
     */
    private ButtonGroup btg_group;

    /**
     * Constructor de la clase frmMenu.
     * 
     * @param obj_gestorLP El gestor de lógica de negocio asociado a la ventana.
     */
    public frmMenu(clsGestorLN obj_gestorLP)
    {

        obj_gestor = obj_gestorLP;

        img_Icon = new ImageIcon(getClass().getResource
        ("/lp/imagen/love.jpg")).getImage();
        setIconImage(img_Icon);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(600, 180, 820, 500);

        panel_panelManejo = new JPanel();

        panel_panelManejo.setBounds(10, 10, 100, 100);
        panel_panelManejo.setBackground(new Color(115, 198, 182));
        setContentPane(panel_panelManejo);
        panel_panelManejo.setLayout(null);

        txt_NombreTxoko = new JTextField();
        txt_NombreTxoko.setBounds(50, 58, 144, 20);
        txt_NombreTxoko.setColumns(1);
        panel_panelManejo.add(txt_NombreTxoko);

        lbl_NombreTxoko = new JLabel("Nombre del Txoko");
        lbl_NombreTxoko.setBounds(50, 42, 144, 14);
        panel_panelManejo.add(lbl_NombreTxoko);

        txt_Presidente = new JTextField();
        txt_Presidente.setBounds(50, 98, 144, 20);
        panel_panelManejo.add(txt_Presidente);
        txt_Presidente.setColumns(10);

        lbl_Presidente = new JLabel("Presidente");
        lbl_Presidente.setBounds(50, 82, 144, 14);
        panel_panelManejo.add(lbl_Presidente);

        txt_NumMesas = new JTextField();
        txt_NumMesas.setBounds(50, 138, 144, 20);
        txt_NumMesas.setColumns(2);
        panel_panelManejo.add(txt_NumMesas);

        lbl_NumMesas = new JLabel("Numero de Mesas");
        lbl_NumMesas.setBounds(50, 122, 144, 14);
        panel_panelManejo.add(lbl_NumMesas);

        txt_NombreViejoTxoko = new JTextField();
        txt_NombreViejoTxoko.setBounds(204, 58, 144, 20);
        txt_NombreViejoTxoko.setColumns(1);
        panel_panelManejo.add(txt_NombreViejoTxoko);

        lbl_NombreViejoTxoko = new JLabel("Nombre Viejo del Txoko");
        lbl_NombreViejoTxoko.setBounds(204, 42, 144, 14);
        panel_panelManejo.add(lbl_NombreViejoTxoko);
       
        btn_Aceptar = new JButton("Aceptar");
        btn_Aceptar.addActionListener(this);
        btn_Aceptar.setActionCommand("H");
        btn_Aceptar.setBounds(210, 200, 150, 95);
        btn_Aceptar.setBackground(new Color(74, 239, 241));
        panel_panelManejo.add(btn_Aceptar);

        btn_Alta = new JButton("Alta de Txoko");
        btn_Alta.addActionListener(this);
        btn_Alta.setActionCommand("A");
        btn_Alta.setBounds(50, 200, 150, 25);
        btn_Alta.setBackground(new Color(74, 239, 241));
        panel_panelManejo.add(btn_Alta);

        btn_Baja = new JButton("Baja de Txoko");
        btn_Baja.addActionListener(this);
        btn_Baja.setActionCommand("B");
        btn_Baja.setBounds(50, 235, 150, 25);
        btn_Baja.setBackground(new Color(74, 239, 241));
        panel_panelManejo.add(btn_Baja);

        btn_Modi = new JButton("Modificar Txoko");
        btn_Modi.addActionListener(this);
        btn_Modi.setActionCommand("M");
        btn_Modi.setBounds(50, 270, 150, 25);
        btn_Modi.setBackground(new Color(74, 239, 241));
        panel_panelManejo.add(btn_Modi);

        btg_group = new ButtonGroup();
        btg_group.add(btn_Alta);
        btg_group.add(btn_Baja);
        btg_group.add(btn_Modi);

      
        panel_panelTabla = new PanelTablaTxoko();
        setContentPane(panel_panelTabla);

      
        mnb_menuBar = new JMenuBar();
        setJMenuBar(mnb_menuBar);

        /*
         * Creamos el primer JMenu y lo pasamos como parámetro al JMenuBar 
         * mediante el método add
         */
        menu_menu1 = new JMenu("Opciones");

        menu_menu1.addMouseListener(new MouseAdapter()
        {
            public void mouseEntered(MouseEvent e)
            {
                if (!menu_menu1.isSelected())
                {
                    menu_menu1.setBackground(new Color(30, 180, 30));
                    menu_menu1.setForeground(new Color(0, 0, 0));
                }
            }

            public void mouseExited(MouseEvent e)
            {
                if (!menu_menu1.isSelected())
                {
                    menu_menu1.setBackground(new Color(255, 255, 255));
                    menu_menu1.setForeground(new Color(30, 137, 30));
                }
            }

            public void mousePressed(MouseEvent e)
            {
                if (menu_menu1.isSelected())
                {
                    menu_menu1.setBackground(new Color(30, 137, 30));
                    menu_menu1.setForeground(new Color(255, 255, 255));
                } else
                {
                    menu_menu1.setBackground(new Color(30, 180, 30));
                    menu_menu1.setForeground(new Color(255, 255, 255));
                }
            }
        });
        mnb_menuBar.add(menu_menu1);

        /*
         * Creamos el segundo y tercer objetos de la clase JMenu y los 
         * asociamos con el primer JMenu creado
         */
        menu_menu2 = new JMenu("Cambiar vista");
        menu_menu1.add(menu_menu2);
        menu_menu3 = new JMenu("Salir");
        menu_menu1.add(menu_menu3);

        /*
         * Creamos los dos primeros objetos de la clase JMenuItem y los 
         * asociamos con el segundo JMenu
         */
        menui_menuItem21 = new JMenuItem("Añadir Entrada");
        menu_menu2.add(menui_menuItem21);
        menui_menuItem21.addActionListener(this);
        menui_menuItem22 = new JMenuItem("Quitar Entrada");
        menu_menu2.add(menui_menuItem22);
        menui_menuItem22.addActionListener(this);

        /*
         * Creamos los otros dos objetos de la clase JMenuItem y los asociamos 
         * con el tercer JMenu
         */
        menui_menuItem31 = new JMenuItem("Exit");
        menu_menu3.add(menui_menuItem31);
        menui_menuItem31.addActionListener(this);

        initPantalla();

    }

    /**
     * Clase interna que representa un panel con una tabla de txokos.
     */
    public class PanelTablaTxoko extends JPanel
    {

        /**
         * Panel con una tabla de txokos.
         */
        public PanelTablaTxoko()
        {
            super(new GridLayout(1, 0));
            JScrollPane scr_scrollPane;
            String[] columnNames =
            { "Nombre Txoko", "Presidente", "NumMesas" };

            actualizarTabla();

            mod_model = new DefaultTableModel(obj_data, columnNames)
            {
                @Override
                public boolean isCellEditable(int row, int column)
                {
                    return false;
                }
            };
            tbl_Txokos = new JTable(mod_model);
            tbl_Txokos.setBackground(new Color(93, 173, 226));
            tbl_Txokos.setCellEditor(null);

            tbl_Txokos.addMouseListener(new MouseAdapter()
            {
                public void mouseClicked(MouseEvent e)
                {
                    printData(tbl_Txokos);
                }
            });

            scr_scrollPane = new JScrollPane(tbl_Txokos);
            add(scr_scrollPane);
            setOpaque(true); 
            add(panel_panelManejo);
        }

        /**
         * Actualiza los datos de la tabla de txokos.
         */
        public void actualizarTabla()
        {
            arl_Txokos = obj_gestor.arrl_mostrarTxokosOrdenados();
            int int_tuplas = arl_Txokos.size();
            itfProperty obj_txoko;

            obj_data = new Object[int_tuplas][3];
            for (int i = 0; i < arl_Txokos.size(); i++)
            {
                obj_txoko = arl_Txokos.get(i);

                obj_data[i] = new Object[]
                { obj_txoko.getProperty(STR_TXOKO), obj_txoko.getProperty
                        (STR_PRESIDENTE_TXOKO),
                        obj_txoko.getProperty(STR_MESAS) };
            }
        }
    }

    /**
     * Muestra los datos del txoko seleccionado en la tabla en los campos de 
     * texto.
     * 
     * @param tblTxokos La tabla de txokos.
     */
    private void printData(JTable tblTxokos)
    {
        int int_rows = tblTxokos.getSelectedRow();
        String[] str_arlStringsTxoko = new String[3];

        javax.swing.table.TableModel model = tblTxokos.getModel();

        for (int j = 0; j < 3; j++)
        {
            str_arlStringsTxoko[j] = model.getValueAt(int_rows, j).toString();
        }
        txt_NombreTxoko.setText((str_arlStringsTxoko[0]));
        txt_Presidente.setText((str_arlStringsTxoko[1]));
        txt_NumMesas.setText((str_arlStringsTxoko[2]));

    }

    /**
     * Conjunto de parametros utilizados para dar forma a la pantalla
     * 
     */
    private void initPantalla()
    {
        img_Icon = new ImageIcon(getClass().getResource
        ("/lp/imagen/love.jpg")).getImage();
        setIconImage(img_Icon);
        setTitle("JMenu"); 
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(600, 180, 820, 500);
        setVisible(true);
    }

    /**
     * Metodo empleado para refescar la pantalla
     * 
     */
    private void actualizarPantalla()
    {
        revalidate();
        repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        String str_Presidente;
        String str_Nombre;
        String str_ViejoNombre;
        int int_Mesa;

        if (e.getSource() == menui_menuItem21)
        {
            add(panel_panelManejo);
            actualizarPantalla();
        }
        if (e.getSource() == menui_menuItem22)
        {
            remove(panel_panelManejo);
            actualizarPantalla();
        }
        if (e.getSource() == menui_menuItem31)
        {
            setVisible(false);
        }

        switch (e.getActionCommand())
        {
        case "A":
            int_caso = 1;

            btn_Alta.setBackground(new Color(74, 241, 107));
            btn_Baja.setBackground(new Color(74, 239, 241));
            btn_Modi.setBackground(new Color(74, 239, 241));

            txt_Presidente.setEnabled(true);
            txt_NumMesas.setEnabled(true);
            txt_NombreTxoko.setEnabled(true);
            txt_NombreViejoTxoko.setEnabled(false);

            txt_Presidente.setBackground(new Color(151, 206, 243));
            txt_NombreTxoko.setBackground(new Color(151, 206, 243));
            txt_NumMesas.setBackground(new Color(151, 206, 243));
            txt_NombreViejoTxoko.setBackground(Color.red);
            txt_NombreViejoTxoko.setText("");

            break;

        case "B":
            int_caso = 2;

            btn_Alta.setBackground(new Color(74, 239, 241));
            btn_Baja.setBackground(new Color(74, 241, 107));
            btn_Modi.setBackground(new Color(74, 239, 241));

            txt_Presidente.setEnabled(false);
            txt_NumMesas.setEnabled(false);
            txt_NombreTxoko.setEnabled(true);
            txt_NombreViejoTxoko.setEnabled(false);

            txt_Presidente.setBackground(new Color(151, 206, 243));
            txt_NombreTxoko.setBackground(new Color(151, 206, 243));
            txt_NumMesas.setBackground(new Color(151, 206, 243));
            txt_NombreViejoTxoko.setBackground(Color.red);
            txt_NombreViejoTxoko.setText("");

            break;

        case "M":
            int_caso = 3;
            btn_Alta.setBackground(new Color(74, 239, 241));
            btn_Baja.setBackground(new Color(74, 239, 241));
            btn_Modi.setBackground(new Color(74, 241, 107));
            txt_Presidente.setEnabled(true);
            txt_NumMesas.setEnabled(true);
            txt_NombreTxoko.setEnabled(true);
            txt_NombreViejoTxoko.setEnabled(true);

            txt_Presidente.setBackground(new Color(151, 206, 243));
            txt_NombreTxoko.setBackground(new Color(151, 206, 243));
            txt_NumMesas.setBackground(new Color(151, 206, 243));
            txt_NombreViejoTxoko.setBackground(new Color(151, 206, 243));

            break;

        case "H":
            switch (int_caso)
            {
            case 1:
                str_Presidente = txt_Presidente.getText();
                str_Nombre = txt_NombreTxoko.getText();
                int_Mesa = Integer.parseInt(txt_NumMesas.getText());

                obj_gestor.vo_txokoNuevo(str_Presidente, str_Nombre, int_Mesa);

                for (int i = obj_data.length - 1; i >= 0; i--)
                {
                    mod_model.removeRow(i);
                }

                panel_panelTabla.actualizarTabla();

                for (int i = 0; i < obj_data.length; i++)
                {
                    mod_model.addRow(obj_data[i]);
                }

                actualizarPantalla();
                break;

            case 2:
                str_Nombre = txt_NombreTxoko.getText();
                obj_gestor.vo_eliminarTxoko(str_Nombre);

                mod_model.removeRow(tbl_Txokos.getSelectedRow());

                panel_panelTabla.actualizarTabla();
                actualizarPantalla();
                break;

            case 3:
                str_Presidente = txt_Presidente.getText();
                str_Nombre = txt_NombreTxoko.getText();
                int_Mesa = Integer.parseInt(txt_NumMesas.getText());
                str_ViejoNombre = txt_NombreViejoTxoko.getText();
                obj_gestor.vo_modificarTxoko(str_Presidente, str_Nombre, 
                    int_Mesa, str_ViejoNombre);

                for (int i = obj_data.length - 1; i >= 0; i--)
                {
                    mod_model.removeRow(i);
                }

                panel_panelTabla.actualizarTabla();

                for (int i = 0; i < obj_data.length; i++)
                {
                    mod_model.addRow(obj_data[i]);
                }

                actualizarPantalla();

                break;

            default:
                break;
            }
            break;
        }

    }
}