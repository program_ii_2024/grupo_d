package COMUN;

/**
 * Clase que contiene constantes utilizadas para
 * interactuar con la base de datos.
 */
public class clsConstantesBD 
{
	/**
	 * Nombre del campo DNI en la tabla de usuarios.
	 */
	public static final String TABLA_USUARIO_DNI = "str_dni";

	/**
	 * Nombre del campo apellido en la tabla de usuarios.
	 */
	public static final String TABLA_USUARIO_APELLIDO = "str_apellido";

	/**
	 * Nombre del campo rol en la tabla de usuarios.
	 */
	public static final String TABLA_USUARIO_ROL = "en_Rol";

	/**
	 * Nombre del campo nombre en la tabla de usuarios.
	 */
	public static final String TABLA_USUARIO_NOMBRE = "str_nombre";

	/**
	 * Nombre del campo contraseña en la tabla de usuarios.
	 */
	public static final String TABLA_USUARIO_CONTRASENA = "str_contrasena";

	/**
	 * Índice del campo DNI en la tabla de usuarios.
	 */
	public static int USUARIO_DNI = 1;

	/**
	 * Índice del campo apellido en la tabla de usuarios.
	 */
	public static int USUARIO_APELLIDO = 2;

	/**
	 * Índice del campo rol en la tabla de usuarios.
	 */
	public static int USUARIO_ROL = 3;

	/**
	 * Índice del campo nombre en la tabla de usuarios.
	 */
	public static int USUARIO_NOMBRE = 4;

	/**
	 * Índice del campo contraseña en la tabla de usuarios.
	 */
	public static int USUARIO_CONTRASENA = 5;

	/**
	 * Nombre del campo presidente en la tabla de txokos.
	 */
	public static final String TABLA_TXOKO_PRESIDENTE = "str_presidente";

	/**
	 * Nombre del campo txoko en la tabla de txokos.
	 */
	public static final String TABLA_TXOKO_TXOKO = "str_txoko";

	/**
	 * Nombre del campo mesas en la tabla de txokos.
	 */
	public static final String TABLA_TXOKO_MESAS = "int_mesas";

	/**
	 * Índice del campo presidente en la tabla de txokos.
	 */
	public static int TXOKO_PRESIDENTE = 1;

	/**
	 * Índice del campo txoko en la tabla de txokos.
	 */
	public static int TXOKO_TXOKO = 2;

	/**
	 * Índice del campo mesas en la tabla de txokos.
	 */
	public static int TXOKO_MESAS = 3;

	/**
	 * Nombre del campo aforo en la tabla de gimnasios.
	 */
	public static final String TABLA_GIMNASIO_AFORO = "int_aforo";

	/**
	 * Nombre del campo máquinas en la tabla de gimnasios.
	 */
	public static final String TABLA_GIMNASIO_MAQUINAS = "int_maquinas";

	/**
	 * Nombre del campo gimnasio en la tabla de gimnasios.
	 */
	public static final String TABLA_GIMNASIO_GIMNASIO = "str_gimnasio";

	/**
	 * Índice del campo aforo en la tabla de gimnasios.
	 */
	public static int GIMNASIO_AFORO = 1;

	/**
	 * Índice del campo máquinas en la tabla de gimnasios.
	 */
	public static int GIMNASIO_MAQUINAS = 2;

	/**
	 * Índice del campo gimnasio en la tabla de gimnasios.
	 */
	public static int GIMNASIO_GIMNASIO = 3;

	/**
	 * Nombre de la columna para el identificador de la piscina 
	 * en la tabla PISCINA.
	 */
	public static final String TABLA_PISCINA_PISCINA = "str_piscina";

	/**
	 * Nombre de la columna para la temperatura de la piscina 
	 * en la tabla PISCINA.
	 */
	public static final String TABLA_PISCINA_TEMPERATURA = "float_temperatura";

	/** 
	 * Nombre de la columna para el tamaño de la piscina en la tabla PISCINA. 
	 */
	public static final String TABLA_PISCINA_TAMANO = "int_tamano";

	/**
	 * Nombre de la columna para el número de calles de la piscina en la tabla
	 * PISCINA.
	 */
	public static final String TABLA_PISCINA_CALLES = "int_calles";

	/** 
	 * Índice del PreparedStatement para el identificador de la piscina. 
	 */
	public static int PISCINA_PISCINA = 1;

	/** 
	 * Índice del PreparedStatement para la temperatura de la piscina. 
	 */
	public static int PISCINA_TEMPERATURA = 2;

	/** 
	 * Índice del PreparedStatement para el tamaño de la piscina. 
	 */
	public static int PISCINA_TAMANO = 3;

	/** 
	 * Índice del PreparedStatement para el número de calles de la piscina. 
	 */
	public static int PISCINA_CALLES = 4;

	/**
	 * Consulta SQL para insertar un usuario en la tabla de usuarios.
	 */
	public static final String SQL_INSERT_USUARIO = 
		"INSERT INTO `bd_registros`.`usuario` " +
		"(`str_dni`,`str_apellido`,`en_rol`,`str_nombre`,`str_contrasena`) " +
		"VALUES(?, ?, ?, ?, ?);";

	/**
	 * Consulta SQL para insertar un txoko en la tabla de txokos.
	 */
	public static final String SQL_INSERT_TXOKO = 
		"INSERT INTO `bd_registros`.`txoko` " +
		"(`str_presidente`,`str_txoko`,`int_mesas`)" +
		"VALUES (?, ?, ?);";

	/**
	 * Consulta SQL para insertar un gimnasio en la tabla de gimnasios.
	 */
	public static final String SQL_INSERT_GIMNASIO = 
		"INSERT INTO `bd_registros`.`gimnasio` " +
		"(`int_aforo`,`int_maquinas`,`str_gimnasio`)" +
		"VALUES (?, ?, ?);";

	/** 
	 * SQL para insertar una nueva piscina en la base de datos. 
	 */
	public static final String SQL_INSERT_PISCINA = 
		"INSERT INTO `bd_registros`.`piscina` " +
		"(`str_piscina`, `float_temperatura`, `int_tamano`, `int_calles`)" +
		"VALUES (?, ?, ?, ?);";

	/**
	 * Consulta SQL para seleccionar un usuario por su DNI.
	 */
	public static final String SQL_SELECT_USUARIO_BY_DNI = 
		"SELECT `USUARIO`.`str_dni`,`USUARIO`.`str_apellido`, " +
		"`USUARIO`.`en_Rol`, " +
		"`USUARIO`.`str_nombre`, `USUARIO`.`str_contrasena` " +
		"FROM `USUARIO` WHERE USUARIO.str_dni = ? ;";

	/**
	 * Consulta SQL para seleccionar todos los usuarios.
	 */
	public static final String SQL_SELECT_USUARIO = 
		"SELECT USUARIO.str_dni, USUARIO.str_nombre, USUARIO.str_apellido," +
		"USUARIO.str_contrasena, USUARIO.en_Rol " +
		"FROM USUARIO;";

	/**
	 * Consulta SQL para seleccionar todos los txokos.
	 */
	public static final String SQL_SELECT_TXOKO = 
		"SELECT TXOKO.str_presidente, TXOKO.str_txoko, TXOKO.int_mesas " +
		"FROM TXOKO;";

	/**
	 * Consulta SQL para seleccionar todos los gimnasios.
	 */
	public static final String SQL_SELECT_GIMNASIO = 
		"SELECT GIMNASIO.int_aforo, GIMNASIO.int_maquinas, " +
		"GIMNASIO.str_gimnasio " + 
		"FROM GIMNASIO;";

	/** 
	 * SQL para seleccionar todas las piscinas en la base de datos. 
	 */
	public static final String SQL_SELECT_PISCINA = 
		"SELECT PISCINA.str_piscina, PISCINA.float_temperatura, " +
		"PISCINA.int_tamano, PISCINA.int_calles " + 
		"FROM PISCINA;";

	/**
	 * Consulta SQL para eliminar un usuario por su DNI.
	 */
	public static final String SQL_DELETE_USUARIO_BY_DNI = 
		"DELETE FROM `bd_registros`.`usuario` " +
		"WHERE `str_dni` = ?;";

	/**
	 * Consulta SQL para eliminar un txoko por su nombre.
	 */
	public static final String SQL_DELETE_TXOKO_BY_NAME = 
		"DELETE FROM `bd_registros`.`txoko` " +
		"WHERE `str_txoko` = ?;";

	/**
	 * Consulta SQL para eliminar un gimnasio por su nombre.
	 */
	public static final String SQL_DELETE_GIMNASIO_BY_NAME = 
		"DELETE FROM `bd_registros`.`gimnasio` " +
		"WHERE `str_gimnasio` = ?;";

	/** 
	 * SQL para eliminar una piscina por su nombre en la base de datos. 
	 */
	public static final String SQL_DELETE_PISCINA_BY_NAME = 
		"DELETE FROM `bd_registros`.`piscina` " +
		"WHERE `str_piscina` = ?;";

	/**
	 * Consulta SQL para actualizar un usuario por su DNI.
	 */
	public static final String SQL_UPDATE_USUARIO_BY_DNI = 
		"UPDATE `bd_registros`.`usuario` " +
		"SET `str_dni` = ?, " +
		"`str_nombre` = ?, " +
		"`str_apellido` = ?, " +
		"`str_contrasena` = ?, " +
		"`en_Rol` = ? " +
		"WHERE `str_dni` = ?;";

	/**
	 * Consulta SQL para actualizar un txoko por su nombre.
	 */
	public static final String SQL_UPDATE_TXOKO_BY_NAME = 
		"UPDATE `bd_registros`.`TXOKO` " +
		"SET `str_presidente` = ?, " +
		"`str_txoko` = ?, " +
		"`int_mesas` = ? " +
		"WHERE `str_txoko` = ?;";

	/**
	 * Consulta SQL para actualizar un gimnasio por su nombre.
	 */
	public static final String SQL_UPDATE_GIMNASIO_BY_NAME = 
		"UPDATE `bd_registros`.`GIMNASIO` " +
		"SET `int_aforo` = ?, " +
		"`int_maquinas` = ?, " +
		"`str_gimnasio` = ? " +
		"WHERE `str_gimnasio` = ?;";

	/**
	 * SQL para actualizar la información de una piscina en la base de datos.
	 */
	public static final String SQL_UPDATE_PISCINA_BY_NAME = 
		"UPDATE `bd_registros`.`PISCINA` " +
		"SET `str_piscina` = ?, " +
		"`float_temperatura` = ?, " +
		"`int_tamano` = ?, " +
		"`int_calles` = ? " +
		"WHERE `str_piscina` = ?;";
}
