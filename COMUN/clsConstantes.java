package COMUN;

/**
 * Esta clase contiene constantes relacionadas con los roles de usuario.
 */
public class clsConstantes
{
    /**
     * Enumeracion que define los roles de usuario.
     */
    public enum en_Rol
    {
        /**
         * Rol de administrador.
         */
        en_Rol_ADMIN(1),

        /**
         * Rol de solo lectura.
         */
        en_Rol_READ(2),

        /**
         * Rol de lectura y escritura.
         */
        en_Rol_READ_WRITE(3),

        /**
         * Rol no valido.
         */
        en_Rol_INVALID(4);

        /**
         * Entero al que le asignamos el valor del rol
         */
        private int int_value;

        /**
         * Metodo que asigna el valor de rol
         * 
         * @param int_tipo Numero del rol
         */
        private en_Rol(int int_tipo)
        {
            this.int_value = int_tipo;
        }
    }

    /**
     * Enumeracion que define los turnos del dia.
     */
    public static enum en_Turno
    {
        /**
         * Turno de la mañana.
         */
        en_Turno_TM,
        /**
         * Turno de la tarde.
         */
        en_Turno_TT,
        /**
         * Turno de la noche.
         */
        en_Turno_TN
    }

    /**
     * Enumeracion que define los dias de la semana.
     */
    public static enum en_Dia
    {
        /**
         * Lunes.
         */
        en_Dia_LUNES,
        /**
         * Martes.
         */
        en_Dia_MARTES,
        /**
         * Miercoles.
         */
        en_Dia_MIERCOLES,
        /**
         * Jueves.
         */
        en_Dia_JUEVES,
        /**
         * Viernes.
         */
        en_Dia_VIERNES
    }

    /**
     * Clave de identificación para el DNI de un usuario.
     */
    public static final String STR_DNI = "str_dni";

    /**
     * Clave para el nombre de un usuario.
     */
    public static final String STR_NOMBRE = "str_nombre";

    /**
     * Clave para la contraseña de un usuario.
     */
    public static final String STR_CONTRASENA = "str_contrasena";

    /**
     * Clave para el apellido de un usuario.
     */
    public static final String STR_APELLIDO = "str_apellido";

    /**
     * Clave para identificar un 'txoko' (lugar de reunión).
     */
    public static final String STR_TXOKO = "str_txoko";

    /**
     * Clave para identificar al presidente del 'txoko'.
     */
    public static final String STR_PRESIDENTE_TXOKO = "str_presidente";

    /**
     * Clave para el número de mesas en un 'txoko' o similar.
     */
    public static final String STR_MESAS = "int_mesas";

    /**
     * Clave para identificar un gimnasio.
     */
    public static final String STR_GIMNASIO = "str_gimansio";

    /**
     * Clave para el aforo máximo permitido en el gimnasio.
     */
    public static final String STR_AFORO_GIMNASIO = "int_aforo";

    /**
     * Clave para el número de máquinas disponibles en un gimnasio.
     */
    public static final String STR_MAQUINAS_GIMNASIO = "int_maquinas";

    /**
     * Clave para identificar una piscina.
     */
    public static final String STR_PISCINA = "str_piscina";

    /**
     * Clave para el tamaño de la piscina en metros cuadrados.
     */
    public static final String STR_TAMANO_PICINA = "int_tamano";

    /**
     * Clave para el número de calles en una piscina.
     */
    public static final String STR_CALLES_PISCINA = "str_calles";
}
