package COMUN;

import COMUN.clsConstantes.en_Rol;

/**
 * Interfaz para obtener propiedades y roles de usuario.
 */

public interface itfProperty
{
     /**
      * Obtiene el valor de una propiedad específica.
      * 
      * @param str_propiedad La propiedad de la que se desea obtener el valor.
      * @return El valor de la propiedad.
      */
     public Object getProperty(String str_propiedad);

     /**
      * Obtiene el rol de usuario.
      * 
      * @return El rol de usuario.
      */
     public en_Rol get_en_Rol();
}
