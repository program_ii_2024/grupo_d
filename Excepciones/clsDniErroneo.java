package Excepciones;

/**
 * Excepcion lanzada cuando se introduce un DNI incorrecto durante un alta. Esta
 * excepcion se maneja cuando un usuario introduce un numero de DNI que no es
 * valido.
 */
public class clsDniErroneo extends Exception
{
    /**
     * DNI del usuario.
     */
    private String str_dni;

    /**
     * Constructor de la excepcion que recoge el DNI incorrecto.
     *
     * @param str_dni El DNI incorrecto introducido por el usuario.
     */
    public clsDniErroneo(String str_dni)
    {
        this.str_dni = str_dni;
    }

    /**
     * Obtiene el mensaje de error para esta excepcion.
     *
     * @return El mensaje de error que indica que el DNI
     * 
     * introducido no es valido.
     */
    @Override
    public String getMessage()
    {
        return "El dni introducido = " + this.str_dni + " NO ES VALIDO" + '\n' 
            + "No se ha podido completar el ingreso"
            + '\n' + "Por favor, introduzca un numero de DNI valido.";
    }
}