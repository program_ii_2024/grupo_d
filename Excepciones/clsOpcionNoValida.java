package Excepciones;

/**
 * Excepcion lanzada cuando se elige una opcion no valida. Esta excepcion se
 * lanza automaticamente cuando no está esa propiedad del get property No se
 * maneja explicitamente y cierra el sistema por seguridad.
 */
public class clsOpcionNoValida extends RuntimeException
{
     /**
      * opción incorrecta.
      */
     private String str_mensajedeerror;

     /**
      * Constructor de la excepcion que recibe el mensaje incorrecto.
      *
      * @param str_mensaje mensaje de error
      */
     public clsOpcionNoValida(String str_mensaje)
     {
          this.str_mensajedeerror = str_mensaje;
     }

     /**
      * Obtiene el mensaje de error para esta excepcion.
      *
      * @return El mensaje de error que indica que el case no se encuentra 
      * entre las disponibles y que el sistema se cierra por seguridad
      */
     @Override
     public String getMessage()
     {
          return this.str_mensajedeerror;

     }
}
