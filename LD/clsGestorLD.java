package LD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import COMUN.clsConstantes.en_Rol;
import static COMUN.clsConstantesBD.SQL_INSERT_USUARIO;
import static COMUN.clsConstantesBD.SQL_SELECT_GIMNASIO;
import static COMUN.clsConstantesBD.SQL_SELECT_PISCINA;
import static COMUN.clsConstantesBD.SQL_DELETE_USUARIO_BY_DNI;
import static COMUN.clsConstantesBD.SQL_INSERT_GIMNASIO;
import static COMUN.clsConstantesBD.SQL_INSERT_PISCINA;
import static COMUN.clsConstantesBD.SQL_SELECT_USUARIO;
import static COMUN.clsConstantesBD.SQL_UPDATE_GIMNASIO_BY_NAME;
import static COMUN.clsConstantesBD.SQL_UPDATE_PISCINA_BY_NAME;
import static COMUN.clsConstantesBD.SQL_UPDATE_USUARIO_BY_DNI;
import static COMUN.clsConstantesBD.SQL_INSERT_TXOKO;
import static COMUN.clsConstantesBD.SQL_DELETE_GIMNASIO_BY_NAME;
import static COMUN.clsConstantesBD.SQL_DELETE_PISCINA_BY_NAME;
import static COMUN.clsConstantesBD.SQL_DELETE_TXOKO_BY_NAME;
import static COMUN.clsConstantesBD.SQL_SELECT_TXOKO;
import static COMUN.clsConstantesBD.SQL_UPDATE_TXOKO_BY_NAME;

/**
 * Clase encargada de la interaccion con la base de datos.
 */

public class clsGestorLD
{
	private static final String URL = "jdbc:mysql://localhost:3306/";
	private static final String SCHEMA = "bd_registros";
	private static final String PARAMS = "?useUnicode=true" 
		+ "&useJDBCCompliantTimezoneShift=true"
		+ "&useLegacyDatetimeCode=false" 
		+ "&serverTimezone=UTC&useSSL=false";
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String USER = "root";
	private static final String PASS = "root";

	/**
	 * Objeto para crear la conexion a base de datos.
	 */
	private Connection obj_conn;

	/**
	 * Objeto para crear la consulta a base de datos.
	 */
	private PreparedStatement obj_ps;

	/**
	 * Constructor de la clase que inicializa los atributos a valores 
	 * por defecto.
	 */
	public clsGestorLD()
	{
		obj_conn = null;
		obj_ps = null;
	}

	/**
	 * Metodo para la conexion a la base de datos.
	 */
	public void vo_connect()
	{
		try
		{
			Class.forName(DRIVER);
			obj_conn = DriverManager.getConnection(URL + SCHEMA + PARAMS, 
				USER, PASS);
			System.out.println("Debug: Connected to the database");
		} catch (Exception e)
		{
			System.out.println("Debug: NO CONNECTION ");
		}
	}

	/**
	 * Metodo para la desconexion de la base de datos.
	 */
	public void vo_disconnect()
	{
		try
		{
			obj_conn.close();
			obj_ps.close(); // cerrar el statement tb cierra el resultset.
		} catch (SQLException e)
		{
			/*
			 * Si hay algun problema en la desconexion, no hago nada. 
			 * Se intenta de nuevo en el bloque finally.
			 */
		} finally
		{
			try
			{
				obj_conn.close();
			} catch (Exception e)
			{
				/* no hago nada */
			}
			try
			{
				obj_ps.close();
			} catch (Exception e)
			{
				/* no hago nada */
			}
		}
	}

	/**
	 * Inserta un nuevo usuario en la base de datos con el DNI, nombre, 
	 * apellido, contraseña y rol especificados.
	 *
	 * @param str_dni El numero de DNI del usuario.
	 * @param str_nombre El nombre del usuario.
	 * @param str_apellido El apellido del usuario.
	 * @param str_contrasena La contraseña del usuario.
	 * @param en_RolNuevo El rol del usuario.
	 * @return El numero de DNI del usuario insertado, o una cadena Error si 
	 * no se pudo insertar.
	 */
	public String str_insertarUsuario(String str_dni, String str_nombre, 
		String str_apellido, String str_contrasena,
		en_Rol en_RolNuevo)
	{
		int int_regActualizados;
		String str_retorno;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement(SQL_INSERT_USUARIO, 
				PreparedStatement.RETURN_GENERATED_KEYS);
			this.obj_ps.setString(1, str_dni);
			this.obj_ps.setString(4, str_nombre);
			this.obj_ps.setString(2, str_apellido);
			this.obj_ps.setString(5, str_contrasena);
			this.obj_ps.setInt(3, en_RolNuevo.ordinal());

			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_dni;
			if (int_regActualizados == 1)
			{
				ResultSet rs = this.obj_ps.getGeneratedKeys();
				if (rs.next())
				{
					str_retorno = rs.getString(1);
				}
			}
		} catch (SQLException e)
		{
			System.out.println("DEBUG: Error al introducir un usuario " 
				+ "en la base de datos: " + SQL_INSERT_USUARIO);
		}
		return str_retorno;
	}

	/**
	 * Inserta un nuevo txoko en la base de datos con el presidente, nombre del
	 * txoko y numero de mesas especificados.
	 *
	 * @param str_presidente El nombre del presidente del txoko.
	 * @param str_txoko El nombre del txoko.
	 * @param int_mesas El numero de mesas en el txoko.
	 * @return El nombre del txoko insertado, o una cadena Error si no se pudo
	 * insertar.
	 */
	public String str_insertarTxokos(String str_presidente, String str_txoko, 
		int int_mesas)
	{
		int int_regActualizados;
		String str_retorno;
		ResultSet rs_resultado;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement(SQL_INSERT_TXOKO, 
				PreparedStatement.RETURN_GENERATED_KEYS);
			this.obj_ps.setString(1, str_presidente);
			this.obj_ps.setString(2, str_txoko);
			this.obj_ps.setInt(3, int_mesas);

			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_txoko;
			if (int_regActualizados == 1)
			{
				rs_resultado = this.obj_ps.getGeneratedKeys();
				if (rs_resultado.next())
				{
					str_retorno = rs_resultado.getString(2);
				}
			}
		} catch (SQLException e)
		{
			System.out.println("DEBUG: Error al introducir un txoko " 
			+ "en la base de datos: " + SQL_INSERT_TXOKO);
		}
		return str_retorno;
	}

	/**
	 * Inserta una nueva piscina en la base de datos con el nombre de la 
	 * piscina, temperatura, tamano y numero de calles especificados.
	 *
	 * @param str_Piscina El nombre del piscina.
	 * @param float_temperatura La temperatura de la piscina
	 * @param int_tamano El tamano en la piscina.
	 * @param int_calles El numero de calles en la piscina.
	 * 
	 * @return El nombre del piscina insertado, o una cadena Error si no se pudo
	 * insertar.
	 */
	public String str_insertarPiscinas(String str_Piscina, 
		float float_temperatura, int int_tamano, int int_calles)
	{
		int int_regActualizados;
		String str_retorno;
		ResultSet rs_restltado;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement(SQL_INSERT_PISCINA, 
				PreparedStatement.RETURN_GENERATED_KEYS);
			this.obj_ps.setString(1, str_Piscina);
			this.obj_ps.setFloat(2, float_temperatura);
			this.obj_ps.setInt(3, int_tamano);
			this.obj_ps.setInt(4, int_calles);

			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_Piscina;
			if (int_regActualizados == 1)
			{
				rs_restltado = this.obj_ps.getGeneratedKeys();
				if (rs_restltado.next())
				{
					str_retorno = rs_restltado.getString(1);
				}
			}
		} catch (SQLException e)
		{
			System.out.println("DEBUG: Error al introducir un txoko " 	
				+ "en la base de datos: " + SQL_INSERT_PISCINA);
		}
		return str_retorno;
	}

	/**
	 * Metodo insertar Gimnasios.
	 * 
	 * @param int_aforo valor el aforo del gimnasio
	 * @param int_maquinas cantidad de maquinas del gimnasio
	 * @param str_gimnasio nombre del gimnasio
	 * 
	 * @return El nombre del gimnasio insertado, o una cadena Error si no se pudo
	 * insertar.
	 */
	public String str_insertarGimnasios(int int_aforo, int int_maquinas, 
		String str_gimnasio)
	{
		int int_regActualizados;
		String str_retorno;
		ResultSet rs_resultado;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement(SQL_INSERT_GIMNASIO, 
				PreparedStatement.RETURN_GENERATED_KEYS);
			this.obj_ps.setInt(1, int_aforo);
			this.obj_ps.setInt(2, int_maquinas);
			this.obj_ps.setString(3, str_gimnasio);

			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_gimnasio;
			if (int_regActualizados == 1)
			{
				rs_resultado = this.obj_ps.getGeneratedKeys();
				if (rs_resultado.next())
				{
					str_retorno = rs_resultado.getString(2);
				}
			}
		} catch (SQLException e)
		{
			System.out
					.println("DEBUG: Error al introducir un Gimnasio " 
						+ "en la base de datos: " + SQL_INSERT_GIMNASIO);
		}
		return str_retorno;
	}

	/**
	 * Elimina un usuario de la base de datos segun su numero de DNI.
	 * 
	 * @param str_dniEliminar El numero de DNI del usuario que se va a eliminar.
	 * @return El numero de DNI del usuario eliminado, o una cadena Error 
	 * si no se pudo eliminar.
	 */
	public String str_eliminarUsuario(String str_dniEliminar)
	{
		int int_regActualizados;
		String str_retorno;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement
				(SQL_DELETE_USUARIO_BY_DNI);
			this.obj_ps.setString(1, str_dniEliminar);
			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_dniEliminar;
			if (int_regActualizados == 1)
			{
				str_retorno = str_dniEliminar;
			}
		} catch (SQLException e)
		{
			System.out.println(
					"DEBUG: Error al dar de baja un usuario " 
						+ "en la base de datos: " + SQL_DELETE_USUARIO_BY_DNI);
		}
		return str_retorno;
	}

	/**
	 * Elimina un txoko de la base de datos segun su nombre.
	 *
	 * @param str_txokoEliminar El nombre del txoko que se va a eliminar.
	 * @return El nombre del txoko eliminado, o una cadena Error si no se pudo
	 * eliminar.
	 */
	public String str_eliminarTxoko(String str_txokoEliminar)
	{
		int int_regActualizados;
		String str_retorno;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement
				(SQL_DELETE_TXOKO_BY_NAME);
			this.obj_ps.setString(1, str_txokoEliminar);
			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_txokoEliminar;
			if (int_regActualizados == 1)
			{
				str_retorno = str_txokoEliminar;
			}
		} catch (SQLException e)
		{
			System.out.println(
					"DEBUG: Error al dar de baja un txoko " 
						+ "en la base de datos: " + SQL_DELETE_TXOKO_BY_NAME);
		}
		return str_retorno;
	}

	/**
	 * Metodo que da de baja al gimnasio seleccionado
	 *
	 * @param str_gimnasioEliminar Gimnasio a eliminar
	 * @return gimnasio finalmente eliminado
	 */
	public String str_eliminarGimnasio(String str_gimnasioEliminar)
	{
		int int_regActualizados;
		String str_retorno;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement
				(SQL_DELETE_GIMNASIO_BY_NAME);
			this.obj_ps.setString(1, str_gimnasioEliminar);
			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_gimnasioEliminar;
			if (int_regActualizados == 1)
			{
				str_retorno = str_gimnasioEliminar;
			}
		} catch (SQLException e)
		{
			System.out.println(
					"DEBUG: Error al dar de baja un gimnasio " 
					+ "en la base de datos: " + SQL_DELETE_GIMNASIO_BY_NAME);
		}
		return str_retorno;

	}

	/**
	 * Metodo que da de baja al piscina seleccionado
	 *
	 * @param str_piscinaEliminar Piscina a eliminar
	 * @return piscina finalmente eliminado
	 */
	public String str_eliminarPiscina(String str_piscinaEliminar)
	{
		int int_regActualizados;
		String str_retorno;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement
				(SQL_DELETE_PISCINA_BY_NAME);
			this.obj_ps.setString(1, str_piscinaEliminar);
			int_regActualizados = this.obj_ps.executeUpdate();
			if (int_regActualizados == 1)
			{
				str_retorno = str_piscinaEliminar;
			}
		} catch (SQLException e)
		{
			System.out.println(
					"DEBUG: Error al dar de baja un gimnasio " 
						+ "en la base de datos: " + SQL_DELETE_PISCINA_BY_NAME);
		}
		return str_retorno;

	}

	/**
	 * Obtiene un ResultSet con el listado de usuarios.
	 *
	 * @return ResultSet con el listado de usuarios.
	 */
	public ResultSet rs_listadoUsuarios()
	{
		ResultSet rs_retorno;
		rs_retorno = null;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement(SQL_SELECT_USUARIO);

			rs_retorno = this.obj_ps.executeQuery();
		} catch (SQLException e)
		{
			System.out.println("Error en la SQL= " 
				+ SQL_SELECT_USUARIO + " " + e);
		}
		return rs_retorno;
	}

	/**
	 * Obtiene un ResultSet con el listado de Txokos.
	 *
	 * @return ResultSet con el listado de Txokos.
	 */
	public ResultSet rs_listadoTxokos()
	{
		ResultSet rs_retorno;
		rs_retorno = null;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement(SQL_SELECT_TXOKO);

			rs_retorno = this.obj_ps.executeQuery();
		} catch (SQLException e)
		{
			System.out.println("Error en la SQL= " 
				+ SQL_SELECT_TXOKO + " " + e);
		}
		return rs_retorno;
	}

	/**
	 * Obtiene un ResultSet con el listado de gimnasios.
	 *
	 * @return ResultSet con el listado de gimnasios.
	 */
	public ResultSet rs_listadoGimasios()
	{
		ResultSet rs_retorno;
		rs_retorno = null;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement(SQL_SELECT_GIMNASIO);

			rs_retorno = this.obj_ps.executeQuery();
		} catch (SQLException e)
		{
			System.out.println("Error en la SQL= " 
				+ SQL_SELECT_GIMNASIO + " " + e);
		}
		return rs_retorno;
	}

	/**
	 * Obtiene un ResultSet con el listado de piscinas.
	 *
	 * @return ResultSet con el listado de piscinas.
	 */
	public ResultSet rs_listadoPiscinas()
	{
		ResultSet rs_retorno;
		rs_retorno = null;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement(SQL_SELECT_PISCINA);

			rs_retorno = this.obj_ps.executeQuery();
		} catch (SQLException e)
		{
			System.out.println("Error en la SQL= " 
				+ SQL_SELECT_PISCINA + " " + e);
		}
		return rs_retorno;
	}

	/**
	 * Actualiza un usuario en la base de datos.
	 *
	 * @param str_dniViejo DNI del usuario antiguo.
	 * @param str_dniNuevo Nuevo DNI del usuario.
	 * @param str_nombre Nuevo nombre del usuario.
	 * @param str_apellido Nuevo apellido del usuario.
	 * @param str_contrasena Nueva contraseña del usuario.
	 * @param en_rolUsuario Nuevo rol del usuario.
	 * @return El ID del usuario actualizado.
	 */
	public String str_updateUsuario(String str_dniViejo, String str_dniNuevo, 
		String str_nombre, String str_apellido,
		String str_contrasena, en_Rol en_rolUsuario)
	{
		int int_regActualizados;
		String str_retorno;
		ResultSet rs_resultado;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement
				(SQL_UPDATE_USUARIO_BY_DNI,
				PreparedStatement.RETURN_GENERATED_KEYS);
			this.obj_ps.setString(1, str_dniNuevo);
			this.obj_ps.setString(2, str_nombre);
			this.obj_ps.setString(3, str_apellido);
			this.obj_ps.setString(4, str_contrasena);
			this.obj_ps.setInt(5, en_rolUsuario.ordinal() + 1);
			this.obj_ps.setString(6, str_dniViejo);

			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_dniNuevo;
			if (int_regActualizados == 1)
			{
				rs_resultado = this.obj_ps.getGeneratedKeys();
				if (rs_resultado.next())
				{
					str_retorno = rs_resultado.getString(1);
				}
			}
		} catch (SQLException e)
		{
			System.out.println( "DEBUG: Error al introducir un usuario " 
				+ "en la base de datos: " + SQL_UPDATE_USUARIO_BY_DNI);
		}
		return str_retorno;
	}

	/**
	 * Actualiza un Txoko en la base de datos.
	 *
	 * @param str_presidente_nuevo Nuevo presidente del Txoko.
	 * @param str_txoko_nuevo Nuevo nombre del Txoko.
	 * @param int_mesas_nuevo Nuevo numero de mesas en el Txoko.
	 * @param str_txoko_viejo Nombre antiguo del Txoko.
	 * @return El ID del Txoko actualizado.
	 */
	public String str_updateTxoko(String str_presidente_nuevo, 
		String str_txoko_nuevo, int int_mesas_nuevo,
		String str_txoko_viejo)
	{
		int int_regActualizados;
		String str_retorno;
		ResultSet rs_resultado;
		int_regActualizados = 0;
		str_retorno = "Error";
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement
				(SQL_UPDATE_TXOKO_BY_NAME,
				PreparedStatement.RETURN_GENERATED_KEYS);
			this.obj_ps.setString(1, str_presidente_nuevo);
			this.obj_ps.setString(2, str_txoko_nuevo);
			this.obj_ps.setInt(3, int_mesas_nuevo);
			this.obj_ps.setString(4, str_txoko_viejo);

			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_txoko_nuevo;
			if (int_regActualizados == 1)
			{
				rs_resultado = this.obj_ps.getGeneratedKeys();
				if (rs_resultado.next())
				{
					str_retorno = rs_resultado.getString(2);
				}
			}
		} catch (SQLException e)
		{
			System.out.println("DEBUG: Error al modificar un txoko " 
				+ "en la base de datos: " + SQL_UPDATE_TXOKO_BY_NAME);
		}
		return str_retorno;
	}

	/**
	 * Actualiza un gimnasio en la base de datos.
	 *
	 * @param int_aforo_nuevo Nuevo aforo del gimnasio.
	 * @param int_maquinas_nuevo Nuevo numero de maquinas en el gimnasio.
	 * @param str_gimnasio_nuevo Nuevo nombre del gimnasio.
	 * @param str_gimnasio_viejo Nombre antiguo del gimnasio.
	 * @return El ID del gimnasio actualizado.
	 */
	public String str_updateGimnasio(int int_aforo_nuevo, 
		int int_maquinas_nuevo, String str_gimnasio_nuevo,
		String str_gimnasio_viejo)
	{
		int int_regActualizados;
		String str_retorno;
		ResultSet rs_resultado;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement
				(SQL_UPDATE_GIMNASIO_BY_NAME,
				PreparedStatement.RETURN_GENERATED_KEYS);
			this.obj_ps.setInt(1, int_aforo_nuevo);
			this.obj_ps.setInt(2, int_maquinas_nuevo);
			this.obj_ps.setString(3, str_gimnasio_nuevo);
			this.obj_ps.setString(4, str_gimnasio_viejo);

			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_gimnasio_nuevo;
			if (int_regActualizados == 1)
			{
				rs_resultado = this.obj_ps.getGeneratedKeys();
				if (rs_resultado.next())
				{
					str_retorno = rs_resultado.getString(2);
				}
			}
		} catch (SQLException e)
		{
			System.out.println("DEBUG: Error al modificar un Gimnasio " 
				+ "en la base de datos: " + SQL_UPDATE_GIMNASIO_BY_NAME);
		}
		return str_retorno;
	}

	/**
	 * Actualiza un piscina en la base de datos.
	 *
	 * @param int_tamano_nuevo Tamano nuevo de la piscina.
	 * @param float_temperatura_nueva Temperatura nueva de la piscina.
	 * @param int_calles_nuevas Numero de calles nievas en el piscina.
	 * @param str_piscina_nueva Nuevo nombre de la piscina.
	 * @param str_Piscina_vieja Nombre antiguo de la piscina.
	 * @return El ID del piscina actualizado.
	 */
	public String str_updatePiscina(String str_piscina_nueva, 
		float float_temperatura_nueva, int int_tamano_nuevo,
		int int_calles_nuevas, String str_Piscina_vieja)
	{
		int int_regActualizados;
		String str_retorno;
		ResultSet rs_resultado;
		str_retorno = "Error";
		int_regActualizados = 0;
		try
		{
			this.obj_ps = this.obj_conn.prepareStatement
				(SQL_UPDATE_PISCINA_BY_NAME,
				PreparedStatement.RETURN_GENERATED_KEYS);
			this.obj_ps.setString(1, str_piscina_nueva);
			this.obj_ps.setFloat(2, float_temperatura_nueva);
			this.obj_ps.setInt(3, int_tamano_nuevo);
			this.obj_ps.setInt(4, int_calles_nuevas);
			this.obj_ps.setString(5, str_Piscina_vieja);

			int_regActualizados = this.obj_ps.executeUpdate();
			str_retorno = str_piscina_nueva;
			if (int_regActualizados == 1)
			{
				rs_resultado = this.obj_ps.getGeneratedKeys();
				if (rs_resultado.next())
				{
					str_retorno = rs_resultado.getString(1);
				}
			}
		} catch (SQLException e)
		{
			System.out.println("DEBUG: Error al modificar un Gimnasio " 
				+ "en la base de datos: " + SQL_UPDATE_PISCINA_BY_NAME);
		}
		return str_retorno;
	}
}
