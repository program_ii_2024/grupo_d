-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema BD_REGISTROS
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `BD_REGISTROS` ;

-- -----------------------------------------------------
-- Schema BD_REGISTROS
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `BD_REGISTROS` DEFAULT CHARACTER SET utf8 ;
USE `BD_REGISTROS` ;

-- -----------------------------------------------------
-- Table `BD_REGISTROS`.`USUARIO`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BD_REGISTROS`.`USUARIO` ;

CREATE TABLE IF NOT EXISTS `BD_REGISTROS`.`USUARIO` (
  `str_nombre` VARCHAR(60) NOT NULL,
  `str_apellido` VARCHAR(100) NOT NULL,
  `str_dni` VARCHAR(9) NOT NULL,
  `str_contrasena` VARCHAR(100) NOT NULL,
  en_rol ENUM('en_Rol_ADMIN','en_Rol_READ','en_Rol_READ_WRITE','en_Rol_INVALID'),
PRIMARY KEY (`str_dni`)
 )
 ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `mydb`.`PROFESOR`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BD_REGISTROS`.`TXOKO`;

CREATE TABLE IF NOT EXISTS `BD_REGISTROS`.`TXOKO` (
  `str_presidente` VARCHAR(100) NOT NULL,
  `str_txoko` VARCHAR(100) NOT NULL,
  `int_mesas` INT NOT NULL,
  PRIMARY KEY (`str_txoko`)
  )
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `mydb`.`PISCINA`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BD_REGISTROS`.`PISCINA` ;

CREATE TABLE IF NOT EXISTS `BD_REGISTROS`.`PISCINA` (
  `str_piscina` VARCHAR(45) NOT NULL,
  `float_temperatura` FLOAT NOT NULL,
  `int_tamano` INT NOT NULL,
  `int_calles` INT NOT NULL,
  PRIMARY KEY (`str_piscina`))
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `mydb`.`MATRICULA`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BD_REGISTROS`.`GIMNASIO`;

CREATE TABLE IF NOT EXISTS `BD_REGISTROS`.`GIMNASIO` (
   `int_aforo` INT NOT NULL,
  `int_maquinas` INT NOT NULL,
  `str_gimnasio` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`str_gimnasio`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `BD_REGISTROS`.`RESERVAS` (
   `str_dniReserva` VARCHAR(100) NOT NULL,
   `str_nombreEstablecimiento` VARCHAR(100) NOT NULL,
	en_Turno ENUM('en_Turno_TM','en_Turno_TT','en_Turno_TN'),
    en_Dia ENUM('en_Dia_LUNES','en_Dia_MARTES','en_Dia_MIERCOLES','en_Dia_JUEVES','en_Dia_VIERNES', 'en_Dia_SABADO', 'en_Dia_DOMINGO' ),
  PRIMARY KEY (`str_nombreEstablecimiento`, en_Turno, en_Dia)
  )
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

/* Introducción de clases manualmente para tener en la base de datos*/

INSERT INTO `BD_REGISTROS`.`USUARIO` VALUES ('admin', 'admin', 'admin', 'admin', 'en_Rol_ADMIN');
INSERT INTO `BD_REGISTROS`.`USUARIO` VALUES ('pepe', 'garcia', '123456789', '123', 'en_Rol_READ_WRITE');
INSERT INTO `BD_REGISTROS`.`USUARIO` VALUES ('juan', 'hernandez', '987654321', '321', 'en_Rol_READ');

INSERT INTO `BD_REGISTROS`.`TXOKO` VALUES ("David", "Txoko 3", 5);
INSERT INTO `BD_REGISTROS`.`TXOKO` VALUES ("Alfredo", "Txoko 4", 7);
INSERT INTO `BD_REGISTROS`.`TXOKO` VALUES ("Asier", "Txoko 1", 10);
INSERT INTO `BD_REGISTROS`.`TXOKO` VALUES ("Ruben", "Txoko 2", 20);
INSERT INTO `BD_REGISTROS`.`TXOKO` VALUES ("Julen", "Txoko 5", 7);

INSERT INTO `BD_REGISTROS`.`GIMNASIO` VALUES(350,30,'Dream fit');
INSERT INTO `BD_REGISTROS`.`GIMNASIO` VALUES(500,89,'Mendizorroza');
INSERT INTO `BD_REGISTROS`.`GIMNASIO` VALUES(123,12,'Salburua');

INSERT INTO `bd_registros`.`piscina`  VALUES ('Norte', 28.5, 50, 8);
INSERT INTO `bd_registros`.`piscina`  VALUES ('Central', 27.0, 25, 4);
INSERT INTO `bd_registros`.`piscina`  VALUES ('Olimpica', 26.0, 50, 10);